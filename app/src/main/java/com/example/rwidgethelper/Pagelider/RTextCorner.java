package com.example.rwidgethelper.Pagelider;

import com.example.rwidgethelper.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.app.AbilityContext;

public class RTextCorner implements ComponentOwner {
    private Component root;

    public RTextCorner(AbilityContext myContext) {
        init(myContext);
    }

    private void init(AbilityContext myContext) {
        LayoutScatter scatter = LayoutScatter.getInstance(myContext);
        root = scatter.parse(ResourceTable.Layout_text_corner, null, false);
    }

    @Override
    public Component getComponent() {
        return root;
    }

    @Override
    public void instantiateComponent() { }
}
