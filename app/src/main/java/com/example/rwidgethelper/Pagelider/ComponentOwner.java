package com.example.rwidgethelper.Pagelider;

import ohos.agp.components.Component;

public interface ComponentOwner {
    /**
     * 获取存放的component
     *
     * @return 获取Component
     */
    Component getComponent();

    /**
     * 当包含的component被添加到容器时回调
     */
    void instantiateComponent();
}
