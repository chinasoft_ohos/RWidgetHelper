package com.example.rwidgethelper.Pagelider;

import com.example.rwidgethelper.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.app.AbilityContext;

public class RTextVersion implements ComponentOwner {
    private Component root;

    /**
     * 构造
     *
     * @param mContext 上下文
     */
    public RTextVersion(AbilityContext mContext) {
        init(mContext);
    }

    private void init(AbilityContext mContext) {
        LayoutScatter scatter = LayoutScatter.getInstance(mContext);
        root = scatter.parse(ResourceTable.Layout_text_version, null, false);
    }

    @Override
    public Component getComponent() {
        return root;
    }

    @Override
    public void instantiateComponent() {
    }
}
