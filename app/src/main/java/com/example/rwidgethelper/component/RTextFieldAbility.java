package com.example.rwidgethelper.component;

import com.example.rwidgethelper.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.window.service.WindowManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RTextFieldAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        getWindow().setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_rfiedle_text);
        inits();
    }

    private void inits() {
        TextField mTextFieldTv = (TextField) findComponentById(ResourceTable.Id_ed_tv);
        mTextFieldTv.setMaxTextLines(1);
        mTextFieldTv.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                String text = TextFieldFilter(s);
                if (!s.equals(text)) {
                    mTextFieldTv.setText(text);
                }
            }
        });
        TextField mTextFieldCont = (TextField) findComponentById(ResourceTable.Id_ed_cont);
        mTextFieldCont.setMaxTextLines(1);
        mTextFieldCont.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                String text = TextFieldFilter(s);
                if (!s.equals(text)) {
                    mTextFieldCont.setText(text);
                }
            }
        });
        TextField mTextFieldTwo = (TextField) findComponentById(ResourceTable.Id_ed_two);
        mTextFieldTwo.setMaxTextLines(1);
        mTextFieldTwo.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                String text = TextFieldFilter(s);
                if (!s.equals(text)) {
                    mTextFieldTwo.setText(text);
                }
            }
        });
        TextField mTextFieldFor = (TextField) findComponentById(ResourceTable.Id_ed_for);
        mTextFieldFor.setMaxTextLines(1);
        mTextFieldFor.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                String text = TextFieldFilter(s);
                if (!s.equals(text)) {
                    mTextFieldFor.setText(text);
                }
            }
        });
        TextField mTextFieldThread = (TextField) findComponentById(ResourceTable.Id_ed_thread);
        mTextFieldThread.setMaxTextLines(1);
        mTextFieldThread.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                String text = TextFieldFilter(s);
                if (!s.equals(text)) {
                    mTextFieldCont.setText(text);
                }
            }
        });
    }

    private String TextFieldFilter(String s) {
        String regEx = "[^0-9]";
        Pattern compile = Pattern.compile(regEx);
        Matcher matcher = compile.matcher(s);
        return matcher.replaceAll("").trim();
    }
}
