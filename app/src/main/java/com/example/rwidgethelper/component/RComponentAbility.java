package com.example.rwidgethelper.component;

import com.example.rwidgethelper.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class RComponentAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_rcomponent_group);
    }
}
