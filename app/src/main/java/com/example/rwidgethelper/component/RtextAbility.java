package com.example.rwidgethelper.component;

import com.example.rwidgethelper.slice.RtextAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class RtextAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RtextAbilitySlice.class.getName());
    }
}
