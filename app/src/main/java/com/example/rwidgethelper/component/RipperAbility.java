package com.example.rwidgethelper.component;

import com.example.rwidgethelper.ResourceTable;
import com.ruffian.library.widget.component.RRadioButton;
import com.ruffian.library.widget.component.RText;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AbsButton;
import ohos.agp.components.Component;

public class RipperAbility extends Ability implements AbsButton.CheckedStateChangedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_ripper);
        RText mTextTag = (RText) findComponentById(ResourceTable.Id_tv_tag);
        RText btnUpdate = (RText) findComponentById(ResourceTable.Id_tv_update);
        RRadioButton rb = (RRadioButton) findComponentById(ResourceTable.Id_btn2);
        RRadioButton mRRadioButton = (RRadioButton) findComponentById(ResourceTable.Id_btn1);
        mRRadioButton.setCheckedStateChangedListener(this);
        rb.setCheckedStateChangedListener(this);
        rb.setChecked(true);
        btnUpdate.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                boolean enabled = mTextTag.isEnabled();
                mTextTag.setEnabled(!enabled);
                btnUpdate.setText(String.valueOf(!enabled));
            }
        });
    }

    @Override
    public void onCheckedChanged(AbsButton absButton, boolean isClickable) {
        absButton.setClickable(!isClickable);
    }
}
