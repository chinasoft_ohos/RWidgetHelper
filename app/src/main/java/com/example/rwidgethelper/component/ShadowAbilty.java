package com.example.rwidgethelper.component;

import com.example.rwidgethelper.ResourceTable;
import com.ruffian.library.widget.component.RDependentLayout;
import com.ruffian.library.widget.component.RDirectionLayout;
import com.ruffian.library.widget.component.RText;
import com.ruffian.library.widget.utils.ShadowDrawable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayManager;

public class ShadowAbilty extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_shadow_abilty);
        RDependentLayout mRelativeLayout = (RDependentLayout) findComponentById(ResourceTable.Id_layout_ll);
        ShadowDrawable.setShadowDrawable(mRelativeLayout, Color.getIntColor("#ffffff"), dpToPx(8),
                Color.getIntColor("#FF4081"), dpToPx(10), 5, 15);
        RDirectionLayout mRDirectionLayout = (RDirectionLayout) findComponentById(ResourceTable.Id_linear_shadow);
        ShadowDrawable.setShadowDrawable(mRDirectionLayout, Color.getIntColor("#ffffff"), dpToPx(8),
                Color.getIntColor("#DF23DF"), dpToPx(10), 15, 15);
        DirectionalLayout rText = (DirectionalLayout) findComponentById(ResourceTable.Id_text_directional);
        ShadowDrawable.setShadowDrawable(rText, Color.getIntColor("#ffffff"), dpToPx(6),
                Color.getIntColor("#666666"), dpToPx(10), 15, 15);
    }

    private int dpToPx(int dp) {
        float density = DisplayManager.getInstance().getDefaultDisplay(this).get().getAttributes().densityPixels;
        return (int) (dp * density + 0.5f);
    }
}
