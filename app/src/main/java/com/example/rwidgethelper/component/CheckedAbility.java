package com.example.rwidgethelper.component;

import com.example.rwidgethelper.ResourceTable;
import com.ruffian.library.widget.component.RCheckBox;
import com.ruffian.library.widget.component.RRadioButton;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AbsButton;
import ohos.agp.components.RadioContainer;
import ohos.agp.utils.Color;

public class CheckedAbility extends Ability implements RadioContainer.CheckedStateChangedListener,
        AbsButton.CheckedStateChangedListener {
    private RRadioButton mRadioButtonLeft;
    private RRadioButton mRadioButtonCenter;
    private RRadioButton mRadioButtonRight;
    private String mColorBlack = "#000000";
    private String mColorNormal = "#FF4081";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_checked);
        mRadioButtonLeft = (RRadioButton) findComponentById(ResourceTable.Id_btn1_1);
        mRadioButtonCenter = (RRadioButton) findComponentById(ResourceTable.Id_btn2_2);
        mRadioButtonRight = (RRadioButton) findComponentById(ResourceTable.Id_btn3_3);
        RadioContainer mRadioCon = (RadioContainer) findComponentById(ResourceTable.Id_radio_con);
        mRadioCon.setMarkChangedListener(this);
        RCheckBox mCheckBoxHtml = (RCheckBox) findComponentById(ResourceTable.Id_chex_html);
        RCheckBox mCheckBoxOhos = (RCheckBox) findComponentById(ResourceTable.Id_chex_Ohos);
        RCheckBox mCheckBoxIos = (RCheckBox) findComponentById(ResourceTable.Id_chex_ios);
        mCheckBoxHtml.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isSelect) {
                absButton.setChecked(isSelect);
            }
        });

        mCheckBoxIos.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isSelect) {
                absButton.setChecked(isSelect);
            }
        });
        mCheckBoxOhos.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isSelect) {
                absButton.setChecked(isSelect);
            }
        });
        RadioContainer mRadioRab = (RadioContainer) findComponentById(ResourceTable.Id_rab);
        rcbColor(mRadioRab); // 是否选中颜色修改（单选，多选）
        mRadioButtonLeft.setCheckedStateChangedListener(this);
        mRadioButtonCenter.setCheckedStateChangedListener(this);
        mRadioButtonRight.setCheckedStateChangedListener(this);
    }

    private void rcbColor(RadioContainer component) {
        RRadioButton rb1 = (RRadioButton) findComponentById(ResourceTable.Id_btn1);
        RRadioButton rb2 = (RRadioButton) findComponentById(ResourceTable.Id_btn2);
        RRadioButton rb3 = (RRadioButton) findComponentById(ResourceTable.Id_btn3);
        rb1.setCheckedStateChangedListener(this);
        rb2.setCheckedStateChangedListener(this);
        rb3.setCheckedStateChangedListener(this);
        RCheckBox mBoxAd = (RCheckBox) findComponentById(ResourceTable.Id_ctn_ad);
        RCheckBox mBoxAdd = (RCheckBox) findComponentById(ResourceTable.Id_chex_Ohos);
        RCheckBox mBoxHtml = (RCheckBox) findComponentById(ResourceTable.Id_chex_html);
        rb2.setChecked(true); // 默认选中
        mBoxAd.setChecked(true);
        mBoxAdd.setChecked(true);
        mBoxHtml.setChecked(true);
        component.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                if (i == 0) {
                    rb1.setTextColorOn(new Color(Color.getIntColor(mColorNormal)));
                    rb2.setTextColorOff(new Color(Color.getIntColor(mColorBlack)));
                    rb3.setTextColorOff(new Color(Color.getIntColor(mColorBlack)));
                } else if (i == 1) {
                    rb2.setTextColorOn(new Color(Color.getIntColor(mColorNormal)));
                    rb1.setTextColorOff(new Color(Color.getIntColor(mColorBlack)));
                    rb3.setTextColorOff(new Color(Color.getIntColor(mColorBlack)));
                } else if (i == 2) {
                    rb3.setTextColorOn(new Color(Color.getIntColor(mColorNormal)));
                    rb1.setTextColorOff(new Color(Color.getIntColor(mColorBlack)));
                    rb2.setTextColorOff(new Color(Color.getIntColor(mColorBlack)));
                }
            }
        });
    }

    @Override
    public void onCheckedChanged(RadioContainer radioContainer, int i) {
        if (i == 0) {
            mRadioButtonLeft.setChecked(true);
            mRadioButtonCenter.setChecked(false);
            mRadioButtonRight.setChecked(false);
        } else if (i == 1) {
            mRadioButtonLeft.setChecked(false);
            mRadioButtonCenter.setChecked(true);
            mRadioButtonRight.setChecked(false);
        } else if (i == 2) {
            mRadioButtonLeft.setChecked(false);
            mRadioButtonCenter.setChecked(false);
            mRadioButtonRight.setChecked(true);
        }
        mRadioButtonLeft.setTextColorOff(new Color(Color.getIntColor(mColorBlack)));
        mRadioButtonCenter.setTextColorOff(new Color(Color.getIntColor(mColorBlack)));
        mRadioButtonRight.setTextColorOff(new Color(Color.getIntColor(mColorBlack)));
    }

    @Override
    public void onCheckedChanged(AbsButton absButton, boolean isSelect) {
        absButton.setClickable(!isSelect);
    }
}
