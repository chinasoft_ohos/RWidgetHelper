package com.example.rwidgethelper.component;

import com.example.rwidgethelper.ResourceTable;
import com.ruffian.library.widget.component.RImage;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class RimageAbility extends Ability {
    private int corner_radius = 30;
    private int radii = 58;
    private int mRad_lf = 25;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_rimage_com);
        RImage radius = (RImage) findComponentById(ResourceTable.Id_image_radius);
        RImage border = (RImage) findComponentById(ResourceTable.Id_image_border);
        border.setCornerRadius(corner_radius);
        radius.setCornerRadius(corner_radius);
        RImage lr = (RImage) findComponentById(ResourceTable.Id_image_lr);
        RImage left = (RImage) findComponentById(ResourceTable.Id_image_left);
        RImage right = (RImage) findComponentById(ResourceTable.Id_image_right);
        // 0-8顺序为： 左上-》右上 -》右下-》左下
        lr.setCornerRadii(new float[]{radii, radii, radii, radii, radii, radii, 0, 0});
        left.setCornerRadii(new float[]{mRad_lf, mRad_lf, 0, 0, 0, 0, mRad_lf, mRad_lf});
        right.setCornerRadii(new float[]{0, 0, mRad_lf, mRad_lf, mRad_lf, mRad_lf, 0, 0});
    }
}
