package com.example.rwidgethelper.component;

import com.example.rwidgethelper.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

public class RCommonAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_layout_vcomponent);
        ComponentContainer componentById = (ComponentContainer) findComponentById(ResourceTable.Id_vvv);
        componentById.setEnabled(true);
        componentById.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
            }
        });
    }
}
