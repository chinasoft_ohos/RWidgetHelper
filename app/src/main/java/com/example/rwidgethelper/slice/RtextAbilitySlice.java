package com.example.rwidgethelper.slice;

import com.example.rwidgethelper.ResourceTable;
import com.example.rwidgethelper.Pagelider.ComponentOwner;
import com.example.rwidgethelper.Pagelider.RTextCorner;
import com.example.rwidgethelper.Pagelider.RTextState;
import com.example.rwidgethelper.Pagelider.RTextVersion;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSlider;
import ohos.agp.components.PageSliderProvider;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RtextAbilitySlice extends AbilitySlice {
    private List<ComponentOwner> pageList = new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_rtext_com);
        initPages();
    }

    private void initPages() {
        // 数据
        PageData();
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_vp_content);
        pageSlider.setProvider(new PageProvider(pageList, this));
    }

    private void PageData() {
        pageList.add(new RTextCorner(this));
        pageList.add(new RTextState(this));
        pageList.add(new RTextVersion(this));
    }

    // PageProvider的实现
    public class PageProvider extends PageSliderProvider {
        private List<ComponentOwner> components;

        /**
         * 构造
         *
         * @param components component
         * @param context 上下文
         */
        public PageProvider(List<ComponentOwner> components, Context context) {
            this.components = components;
        }

        @Override
        public int getCount() {
            return components.size();
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int index) {
            if (index >= components.size() || componentContainer == null) {
                return Optional.empty();
            }
            ComponentOwner container = components.get(index);
            componentContainer.addComponent(container.getComponent());
            container.instantiateComponent();
            return container.getComponent();
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int index, Object obj) {
            if (index >= pageList.size() || componentContainer == null) {
                return;
            }
            Component content = pageList.get(index).getComponent();
            componentContainer.removeComponent(content);
            return;
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object o) {
            return component == o;
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
