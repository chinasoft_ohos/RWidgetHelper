package com.example.rwidgethelper.slice;

import com.example.rwidgethelper.ResourceTable;
import com.example.rwidgethelper.component.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private boolean btnFlag;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_tv).setClickedListener(this);
        findComponentById(ResourceTable.Id_iv).setClickedListener(this);
        findComponentById(ResourceTable.Id_checked).setClickedListener(this);
        findComponentById(ResourceTable.Id_gradient).setClickedListener(this);
        findComponentById(ResourceTable.Id_ripper).setClickedListener(this);
        findComponentById(ResourceTable.Id_component_group).setClickedListener(this);
        findComponentById(ResourceTable.Id_component).setClickedListener(this);
        findComponentById(ResourceTable.Id_shadow).setClickedListener(this);
        findComponentById(ResourceTable.Id_et).setClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
        btnFlag = false;
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_iv:
                jumpOpen(RimageAbility.class.getName());
                break;
            case ResourceTable.Id_checked:
                jumpOpen(CheckedAbility.class.getName());
                break;
            case ResourceTable.Id_gradient:
                jumpOpen(GradientAbility.class.getName());
                break;
            case ResourceTable.Id_ripper:
                jumpOpen(RipperAbility.class.getName());
                break;
            case ResourceTable.Id_component_group:
                jumpOpen(RComponentAbility.class.getName());
                break;
            case ResourceTable.Id_component:
                jumpOpen(RCommonAbility.class.getName());
                break;
            case ResourceTable.Id_tv:
                jumpOpen(RtextAbility.class.getName());
                break;
            case ResourceTable.Id_shadow:
                jumpOpen(ShadowAbilty.class.getName());
                break;
            case ResourceTable.Id_et:
                jumpOpen(RTextFieldAbility.class.getName());
                break;
        }
    }

    /**
     * 跳转
     *
     * @param abilityName ability页面
     */
    private void jumpOpen(String abilityName) {
        if (!btnFlag) {
            btnFlag = true;
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(abilityName)
                    .build();
            intent.setOperation(operation);
            startAbility(intent);
        }
    }
}
