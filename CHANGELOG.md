## 1.0.0
* 正式版发布

## 0.0.5-SNAPSHOT
* 版本适配

## 0.0.4-SNAPSHOT
* 优化部分library代码

## 0.0.3-SNAPSHOT
* 优化部分demo代码，更新readme

## 0.0.2-SNAPSHOT
* 优化代码

## 0.0.1-SNAPSHOT
 ohos 第一个版本

* openharmony的雷达扫描效果（ShapeElement.SWEEP_GRADIENT_SHADER_TYPE）无法生效，Gradent最后一行第一个效果差异
* 原库使用StateListDrawable完成图片的水波纹效果，openharmony没用对应的api实现，Ripper水波纹和drawableMask有差异
* 因ohos属性不支持，RText字体样式有差异