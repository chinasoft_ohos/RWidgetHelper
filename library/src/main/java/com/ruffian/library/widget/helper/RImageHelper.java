package com.ruffian.library.widget.helper;

import com.ruffian.library.widget.utils.AttrUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import static ohos.agp.components.ComponentState.*;
import static ohos.agp.components.ComponentState.COMPONENT_STATE_EMPTY;

/**
 * ImageView-Helper
 * 已经弃用
 *
 * @author ZhongDaFeng
 */
@Deprecated
public class RImageHelper {
    // 圆角  边框
    private String isCircle = "is_circle";
    private String cornerRadius = "corner_radius";
    private String cornerRadiusTopLeft = "corner_radius_top_left";
    private String cornerRadiusTopRight = "corner_radius_top_right";
    private String cornerRadiusBottomLeft = "corner_radius_bottom_left";
    private String cornerRadiusBottomRight = "corner_radius_bottom_right";
    private String borderWidth = "border_width";
    private String borderColor = "border_color";
    // Icon
    private Element mIconNormal;
    private Element mIconPressed;
    private Element mIconUnable;
    private Element mIconSelected;

    // 圆角
    private float mCorner = -1;
    private float mCornerTopLeft = 0;
    private float mCornerTopRight = 0;
    private float mCornerBottomLeft = 0;
    private float mCornerBottomRight = 0;
    private float mCornerBorderRadii[] = new float[8];
    private float mCornerBitmapRadii[] = new float[8];

    // 边框
    private int mBorderWidth = 0;
    private Color mBorderColor = Color.BLACK;

    // 是否为普通Image(不是圆形或者圆角)
    private boolean mIsNormal = true;

    // 是否圆形
    private boolean mIsCircle = false;

    private final RectFloat mDrawableRect = new RectFloat();
    private final RectFloat mBorderRect = new RectFloat();
    private Paint mBitmapPaint;
    private Paint mBorderPaint;
    private Path mBitmapPath;

    protected int[][] states = new int[4][];
    private StateElement mStateElement;

    private Image mComponent;

    private PixelMapShader mBitmapShader;

    /**
     * 构造
     *
     * @param context 上下文
     * @param image 图
     * @param attrs 属性集
     */
    public RImageHelper(Context context, Image image, AttrSet attrs) {
        mComponent = image;
        initAttributeSet(context, attrs);
    }

    /**
     * 初始化控件属性
     *
     * @param context 上下文
     * @param attrs 属性集
     */
    private void initAttributeSet(Context context, AttrSet attrs) {
        if (context == null || attrs == null) {
            setup();
            return;
        }
        // 基础属性
        mIsCircle = AttrUtils.getBoolean(attrs, isCircle, false);
        mCorner = AttrUtils.getInt(attrs, cornerRadius, -1);
        mCornerTopLeft = AttrUtils.getInt(attrs, cornerRadiusTopLeft, 0);
        mCornerTopRight = AttrUtils.getInt(attrs, cornerRadiusTopRight, 0);
        mCornerBottomLeft = AttrUtils.getInt(attrs, cornerRadiusBottomLeft, 0);
        mCornerBottomRight = AttrUtils.getInt(attrs, cornerRadiusBottomRight, 0);
        mBorderWidth = AttrUtils.getInt(attrs, borderWidth, 0);
        mBorderColor = AttrUtils.getColor(attrs, borderColor, Color.BLACK);
        // 未使用自定义属性容错处理
        if (mIconNormal == null) {
            mIconNormal = mComponent.getImageElement();
        }

        init();

        setup();
    }

    /**
     * 设置
     */
    private void setup() {
        mStateElement = new StateElement();

        /**
         * 设置背景默认值
         */
        if (mIconPressed == null) {
            mIconPressed = mIconNormal;
        }
        if (mIconUnable == null) {
            mIconUnable = mIconNormal;
        }
        if (mIconSelected == null) {
            mIconSelected = mIconNormal;
        }

        // unable,focused,pressed,selected,normal
        states[0] = new int[]{COMPONENT_STATE_DISABLED}; // unable
        states[1] = new int[]{COMPONENT_STATE_PRESSED}; // pressed
        states[2] = new int[]{COMPONENT_STATE_SELECTED}; // selected
        states[3] = new int[]{COMPONENT_STATE_EMPTY}; // normal

        mStateElement.addState(states[0], mIconUnable);
        mStateElement.addState(states[1], mIconPressed);
        mStateElement.addState(states[2], mIconSelected);
        mStateElement.addState(states[3], mIconNormal);

        setIcon();
    }

    /**
     * 初始化设置
     */
    private void init() {
        // 统一设置圆角弧度优先
        updateCornerBorderRadii();
        updateCornerBitmapRadii();

        // 设置圆形，或者某个角度圆角则认为不是普通image
        if (mIsCircle || mCorner > 0 || mCornerTopLeft != 0 || mCornerTopRight != 0 || mCornerBottomRight != 0 || mCornerBottomLeft != 0) {
            mIsNormal = false;
        }

        // border
        if (mBorderPaint == null) {
            mBorderPaint = new Paint();
        }
        mBorderPaint.setStyle(Paint.Style.STROKE_STYLE);
        // bitmap
        if (mBitmapPaint == null) {
            mBitmapPaint = new Paint();
        }
        // BitmapPath
        if (mBitmapPath == null) {
            mBitmapPath = new Path();
        }
    }

    /**
     * 绘制
     *
     * @param canvas 绘制
     */
    public void onDraw(Canvas canvas) {
        /**
         * 绘制图片
         */
        drawBitmap(canvas);

        /**
         * 绘制边框
         */
        drawBorder(canvas);
    }

    /**
     * 绘制bitmap
     *
     * @param canvas 绘制
     */
    private void drawBitmap(Canvas canvas) {
        //drawable
        Element drawable = mComponent.getImageElement();
        if (drawable == null) {
            return;
        }

        // drawable' width & height
        int bmpW = drawable.getWidth();
        int bmpH = drawable.getHeight();

        // ScaleType
        Image.ScaleMode scaleType = mComponent.getScaleMode();

        // 获取bitmap,处理ScaleType
        PixelMap bitmap = getBitmapFromDrawable(drawable, scaleType, bmpW, bmpH, getWidth(), getHeight());

        // 根据 bitmap 创建 BitmapShader
        PixelMapHolder pixelMapHolder = new PixelMapHolder(bitmap);
        mBitmapShader = new PixelMapShader(pixelMapHolder, Shader.TileMode.CLAMP_TILEMODE, Shader.TileMode.CLAMP_TILEMODE);
        // Paint 设置 Shader
        mBitmapPaint.setShader(mBitmapShader, Paint.ShaderType.PIXELMAP_SHADER);

        // 更新各个圆角,根据圆角路径绘制形状
        updateCornerBitmapRadii();
        updateDrawableAndBorderRect();

        if (mIsCircle) { // 圆形
            canvas.drawCircle(getWidth() / 2f, getHeight() / 2f, Math.min(mDrawableRect.getWidth() / 2, mDrawableRect.getWidth() / 2), mBitmapPaint);
        } else { // 圆角
            mBitmapPath.reset(); // must重置
            mBitmapPath.addRoundRect(mDrawableRect, mCornerBitmapRadii, Path.Direction.COUNTER_CLOCK_WISE);
            canvas.drawPath(mBitmapPath, mBitmapPaint);
        }
    }

    /**
     * 获取bitmap,处理ScaleType
     *
     * @param element
     * @param scaleType
     * @param bmpW 图片宽
     * @param bmpH 图片高
     * @param w 控件宽
     * @param h 控件高
     * @return 图片
     */
    private PixelMap getBitmapFromDrawable(Element element, Image.ScaleMode scaleType, int bmpW, int bmpH, int w, int h) {
        /**
         * 支持padding 考虑边框宽度
         */
        int paddingLeft = mComponent.getPaddingLeft() + mBorderWidth;
        int paddingTop = mComponent.getPaddingTop() + mBorderWidth;
        int paddingRight = mComponent.getPaddingRight() + mBorderWidth;
        int paddingBottom = mComponent.getPaddingBottom() + mBorderWidth;

        /**
         * 实际宽高
         */
        float actualW = w - paddingLeft - paddingRight;
        float actualH = h - paddingTop - paddingBottom;

        /**
         * 宽高缩放比例
         */
        float scaleW = actualW / w;
        float scaleH = actualH / h;

        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        Size size = new Size(w, h);
        initializationOptions.size = size;
        PixelMap pixelMap = PixelMap.create(initializationOptions); // 根据text大小创建bitmap
        Texture texture = new Texture(pixelMap);
        Canvas componentCanvas = new Canvas(texture); // 根据 pixelMap 大小创建 canvas 画布
        componentCanvas.translate(paddingLeft, paddingTop); // 移动画布,必须先于缩放，避免误差
        componentCanvas.scale(scaleW, scaleH); // 缩放画布
        element.drawToCanvas(componentCanvas); // 绘制drawable
        return pixelMap;
    }

    private static Matrix.ScaleToFit scaleTypeToScaleToFit(Image.ScaleMode st) {
        /**
         * 根据源码改造  sS2FArray[st.nativeInt - 1]
         */
        switch (st) {
            case STRETCH:
                return Matrix.ScaleToFit.FILL;
            case ZOOM_START:
                return Matrix.ScaleToFit.START;
            case ZOOM_END:
                return Matrix.ScaleToFit.END;
            default:
                return Matrix.ScaleToFit.CENTER;
        }
    }

    /**
     * 绘制边框
     *
     * @param canvas 绘制
     */
    private void drawBorder(Canvas canvas) {
        if (mBorderWidth > 0) {
            // 重新设置 color & width
            mBorderPaint.setColor(mBorderColor);
            mBorderPaint.setStrokeWidth(mBorderWidth);
            if (mIsCircle) {
                float borderRadiusX = (mBorderRect.getWidth() - mBorderWidth) / 2;
                float borderRadiusY = (mBorderRect.getHeight() - mBorderWidth) / 2;
                canvas.drawCircle(getWidth() / 2f, getHeight() / 2f, Math.min(borderRadiusX, borderRadiusY), mBorderPaint);
            } else {
                updateCornerBorderRadii();
                Path path = new Path();
                path.addRoundRect(mBorderRect, mCornerBorderRadii, Path.Direction.CLOCK_WISE);
                canvas.drawPath(path, mBorderPaint);
            }
        }
    }

    /**
     * 更新border Rect
     */
    private void updateDrawableAndBorderRect() {
        float half = mBorderWidth / 2f;
        if (mIsCircle) { // 圆形
            float minRect = Math.min(getWidth(), getHeight());
            mBorderRect.fuse(half, half, getWidth() - half, getHeight() - half); // 边框Rect圆角
            mDrawableRect.fuse(mBorderRect.left + half, mBorderRect.top + half, minRect - half, minRect - half);
        } else { // 圆角
            mBorderRect.fuse(half, half, getWidth() - half, getHeight() - half); // 边框Rect圆角
            mDrawableRect.fuse(mBorderRect.left + half, mBorderRect.top + half, mBorderRect.right - half, mBorderRect.bottom - half);
        }
    }

    /**
     * 更新bitmap圆角弧度
     */
    private void updateCornerBitmapRadii() {
        if (mCorner >= 0) {
            for (int i = 0; i < mCornerBitmapRadii.length; i++) {
                mCornerBitmapRadii[i] = mCorner;
            }
            return;
        }

        if (mCorner < 0) {
            mCornerBitmapRadii[0] = mCornerTopLeft;
            mCornerBitmapRadii[1] = mCornerTopLeft;
            mCornerBitmapRadii[2] = mCornerTopRight;
            mCornerBitmapRadii[3] = mCornerTopRight;
            mCornerBitmapRadii[4] = mCornerBottomRight;
            mCornerBitmapRadii[5] = mCornerBottomRight;
            mCornerBitmapRadii[6] = mCornerBottomLeft;
            mCornerBitmapRadii[7] = mCornerBottomLeft;
            return;
        }
    }

    /**
     * 更新border圆角弧度
     */
    private void updateCornerBorderRadii() {
        if (mCorner >= 0) {
            for (int i = 0; i < mCornerBorderRadii.length; i++) {
                mCornerBorderRadii[i] = mCorner == 0 ? mCorner : mCorner + mBorderWidth;
            }
            return;
        }

        if (mCorner < 0) {
            mCornerBorderRadii[0] = mCornerTopLeft == 0 ? mCornerTopLeft : mCornerTopLeft + mBorderWidth;
            mCornerBorderRadii[1] = mCornerTopLeft == 0 ? mCornerTopLeft : mCornerTopLeft + mBorderWidth;
            mCornerBorderRadii[2] = mCornerTopRight == 0 ? mCornerTopRight : mCornerTopRight + mBorderWidth;
            mCornerBorderRadii[3] = mCornerTopRight == 0 ? mCornerTopRight : mCornerTopRight + mBorderWidth;
            mCornerBorderRadii[4] = mCornerBottomRight == 0 ? mCornerBottomRight : mCornerBottomRight + mBorderWidth;
            mCornerBorderRadii[5] = mCornerBottomRight == 0 ? mCornerBottomRight : mCornerBottomRight + mBorderWidth;
            mCornerBorderRadii[6] = mCornerBottomLeft == 0 ? mCornerBottomLeft : mCornerBottomLeft + mBorderWidth;
            mCornerBorderRadii[7] = mCornerBottomLeft == 0 ? mCornerBottomLeft : mCornerBottomLeft + mBorderWidth;
            return;
        }
    }

    private int getWidth() {
        return mComponent.getWidth();
    }

    private int getHeight() {
        return mComponent.getHeight();
    }

    private void invalidate() {
        mComponent.invalidate();
    }

    /**
     * 颜色
     *
     * @param icon 颜色值
     * @return this
     */

    public RImageHelper setIconNormal(Element icon) {
        this.mIconNormal = icon;
        if (mIconPressed == null) {
            mIconPressed = mIconNormal;
        }
        if (mIconUnable == null) {
            mIconUnable = mIconNormal;
        }
        if (mIconSelected == null) {
            mIconSelected = mIconNormal;
        }
        setIcon();
        return this;
    }

    public Element getIconNormal() {
        return mIconNormal;
    }

    /**
     * 构造
     *
     * @param icon 背景
     * @return 图
     */
    public RImageHelper setIconPressed(Element icon) {
        this.mIconPressed = icon;
        setIcon();
        return this;
    }

    public Element getIconPressed() {
        return mIconPressed;
    }

    /**
     * 构造
     *
     * @param icon 背景
     * @return 图
     */
    public RImageHelper setIconUnable(Element icon) {
        this.mIconUnable = icon;
        setIcon();
        return this;
    }

    public Element getIconUnable() {
        return mIconUnable;
    }

    /**
     * 背景
     *
     * @param icon 图
     * @return 选中的
     */
    public RImageHelper setIconSelected(Element icon) {
        this.mIconSelected = icon;
        setIcon();
        return this;
    }

    public Element getIconSelected() {
        return mIconSelected;
    }

    private void setIcon() {
        mComponent.setImageElement(mStateElement);
        mComponent.invalidate();
    }

    /**
     * 边框宽度
     *
     * @return 宽度
     */

    public int getBorderWidth() {
        return mBorderWidth;
    }

    /**
     * 构造
     *
     * @param borderWidth 边框宽度
     * @return 宽度
     */
    public RImageHelper setBorderWidth(int borderWidth) {
        this.mBorderWidth = borderWidth;
        invalidate();
        return this;
    }

    public Color getBorderColor() {
        return mBorderColor;
    }

    /**
     * 构造
     *
     * @param borderColor 边框颜色
     * @return 颜色
     */
    public RImageHelper setBorderColor(Color borderColor) {
        this.mBorderColor = borderColor;
        invalidate();
        return this;
    }

    /**
     * 角
     *
     * @return 角度
     */
    public float getCorner() {
        return mCorner;
    }

    /**
     * 边框颜色
     *
     * @param corner float类型
     * @return RImageHelper
     */
    public RImageHelper setCorner(float corner) {
        this.mCorner = corner;
        init();
        invalidate();
        return this;
    }

    public float getCornerTopLeft() {
        return mCornerTopLeft;
    }

    /**
     * 构造
     *
     * @param cornerTopLeft 左上
     * @return 图
     */
    public RImageHelper setCornerTopLeft(float cornerTopLeft) {
        this.mCorner = -1;
        this.mCornerTopLeft = cornerTopLeft;
        init();
        invalidate();
        return this;
    }

    public float getCornerTopRight() {
        return mCornerTopRight;
    }

    /**
     * 构造
     *
     * @param cornerTopRight 右上
     * @return 图
     */
    public RImageHelper setCornerTopRight(float cornerTopRight) {
        this.mCorner = -1;
        this.mCornerTopRight = cornerTopRight;
        init();
        invalidate();
        return this;
    }

    public float getCornerBottomLeft() {
        return mCornerBottomLeft;
    }

    /**
     * 构造
     *
     * @param cornerBottomLeft 左下
     * @return 图值
     */
    public RImageHelper setCornerBottomLeft(float cornerBottomLeft) {
        this.mCorner = -1;
        this.mCornerBottomLeft = cornerBottomLeft;
        init();
        invalidate();
        return this;
    }

    public float getCornerBottomRight() {
        return mCornerBottomRight;
    }

    /**
     * 构造
     *
     * @param cornerBottomRight 右下
     * @return 图值
     */
    public RImageHelper setCornerBottomRight(float cornerBottomRight) {
        this.mCorner = -1;
        this.mCornerBottomRight = cornerBottomRight;
        init();
        invalidate();
        return this;
    }

    /**
     * 构造
     *
     * @param topLeft 左上
     * @param topRight 右上
     * @param bottomRight 右下
     * @param bottomLeft 左下
     * @return 综合位置
     */
    public RImageHelper setCorner(float topLeft, float topRight, float bottomRight, float bottomLeft) {
        this.mCorner = -1;
        this.mCornerTopLeft = topLeft;
        this.mCornerTopRight = topRight;
        this.mCornerBottomRight = bottomRight;
        this.mCornerBottomLeft = bottomLeft;
        init();
        invalidate();
        return this;
    }

    /**
     * 是否默认Image
     *
     * @return true是反之
     */
    public boolean isNormal() {
        return mIsNormal;
    }
}
