package com.ruffian.library.widget.component;

import com.ruffian.library.widget.helper.RBaseHelper;
import com.ruffian.library.widget.iface.RHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DependentLayout;
import ohos.app.Context;

/**
 * RRelativeLayout
 *
 * @author ZhongDaFeng
 */
public class RDependentLayout extends DependentLayout implements RHelper<RBaseHelper>  {
    private RBaseHelper mHelper;

    /**
     * 构造
     *
     * @param context 上下文
     */
    public RDependentLayout(Context context) {
        this(context, null);
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrs 属性集
     */
    public RDependentLayout(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrs 属性集
     * @param defStyleAttr 默认
     */
    public RDependentLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mHelper = new RBaseHelper<>(context, this, attrs);
    }

    @Override
    public RBaseHelper getHelper() {
        return mHelper;
    }

    @Override
    public void arrange(int left, int top, int width, int height) {
        super.arrange(left, top, width, height);
        mHelper.onLayout(left, top, width, height);
    }
}
