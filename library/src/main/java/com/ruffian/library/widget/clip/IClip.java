package com.ruffian.library.widget.clip;

import ohos.agp.render.Canvas;

/**
 * IClip
 *
 * @author ZhongDaFeng
 */
public interface IClip {
    /**
     * 图
     *
     * @param canvas 绘画
     */
    public void dispatchDraw(Canvas canvas);

    /**
     * 布局
     *
     * @param left 左边
     * @param top 上边
     * @param right 右边
     * @param bottom 下边
     */
    public void onLayout(int left, int top, int right, int bottom);
}
