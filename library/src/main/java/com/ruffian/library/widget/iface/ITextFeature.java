package com.ruffian.library.widget.iface;

import ohos.agp.components.Component;

/**
 * TextView特性功能接口
 *
 * @author ZhongDaFeng
 */
public interface ITextFeature {
    /**
     * 是否选中
     *
     * @param isEnabled true是，反之
     */
    void setEnabled(boolean isEnabled);

    /**
     * 是否选中
     *
     * @param isSelected true是，反之
     */
    void setSelected(boolean isSelected);

    /**
     * 显示隐藏
     *
     * @param changedComponent 视图
     * @param visibility 显隐
     */
    void onVisibilityChanged(Component changedComponent, int visibility);
}
