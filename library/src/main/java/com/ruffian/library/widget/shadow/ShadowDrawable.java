package com.ruffian.library.widget.shadow;

import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

/**
 * Shadow Drawable
 *
 * @author ZhongDaFeng
 */
public class ShadowDrawable extends Element implements Component.DrawTask {
    private Color mShadowColor; // 阴影颜色
    private float mShadowRadius; // 阴影半径
    private float[] mRoundRadii; // 矩形圆角半径
    private int mShadowDx; // 阴影x偏移
    private int mShadowDy; // 阴影y偏移

    private Path mPath;
    private Paint mPaint;
    private RectFloat mBoundsF;

    /**
     * 构造初始化
     */
    public ShadowDrawable() {
        mPath = new Path();
        mBoundsF = new RectFloat();
        mPaint = new Paint();
    }

    /**
     * 更新颜色圆角阴影
     *
     * @param shadowColor 颜色
     * @param shadowRadius 圆角
     * @param shadowDx x
     * @param shadowDy y
     * @param roundRadii 圆角
     */
    public void updateParameter(Color shadowColor, float shadowRadius, int shadowDx, int shadowDy, float[] roundRadii) {
        this.mShadowColor = shadowColor;
        if (roundRadii != null) {
            this.mRoundRadii = roundRadii.clone();
        }
        this.mShadowRadius = shadowRadius;
        this.mShadowDx = shadowDx;
        this.mShadowDy = shadowDy;

        mPaint.setColor(mShadowColor);
        BlurDrawLooper blurDrawLooper = new BlurDrawLooper(mShadowRadius,mShadowDx, mShadowDy, mShadowColor);
        mPaint.setBlurDrawLooper(blurDrawLooper);
    }

    @Override
    public void setBounds(Rect bounds) {
        super.setBounds(bounds);
        if (bounds.right - bounds.left > 0 && bounds.bottom - bounds.top > 0) {
            updateBounds(bounds);
        }
    }

    /**
     * 更新Bounds
     *
     * @param bounds 角度
     */
    private void updateBounds(Rect bounds) {
        if (bounds == null) {
            bounds = getBounds();
        }
        float left = bounds.left + getShadowOffset() + Math.abs(mShadowDx);
        float right = bounds.right - getShadowOffset() - Math.abs(mShadowDx);
        float top = bounds.top + getShadowOffset() + Math.abs(mShadowDy);
        float bottom = bounds.bottom - getShadowOffset() - Math.abs(mShadowDy);

        mBoundsF.fuse(left, top, right, bottom);
        mPath.reset(); // must重置
        mPath.addRoundRect(mBoundsF, mRoundRadii, Path.Direction.CLOCK_WISE);
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    public float getShadowOffset() {
        return mShadowRadius;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawPath(mPath, mPaint);
    }
}
