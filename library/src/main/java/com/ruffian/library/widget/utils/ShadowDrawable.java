/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ruffian.library.widget.utils;

import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;

public class ShadowDrawable extends ShapeElement {
    /**
     * 阴影
     */
    public final static int SHAPE_ROUND = 1;
    /**
     * 阴影
     */
    public final static int SHAPE_CIRCLE = 2;
    private Paint mShadowPaint;
    private Paint mBgPaint;
    private int mShadowRadius;
    private int mShape;
    private int mShapeRadius;
    private int mOffsetX;
    private int mOffsetY;
    private int[] mBgColor;
    private RectFloat mRect;

    private ShadowDrawable(int shape, int[] bgColor, int shapeRadius, int shadowColor,
                           int shadowRadius, int offsetX, int offsetY) {
        this.mShape = shape;
        this.mBgColor = bgColor;
        this.mShapeRadius = shapeRadius;
        this.mShadowRadius = shadowRadius;
        this.mOffsetX = offsetX;
        this.mOffsetY = offsetY;

        mShadowPaint = new Paint();
        mShadowPaint.setColor(new Color(shadowColor));
        mShadowPaint.setAntiAlias(true);
        mShadowPaint.setMaskFilter(new MaskFilter(shadowRadius, MaskFilter.Blur.NORMAL));
        mShadowPaint.setBlurDrawLooper(new BlurDrawLooper(shadowRadius, mOffsetX, mOffsetY, new Color(shadowColor)));
        mBgPaint = new Paint();
        mBgPaint.setAntiAlias(true);
    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        mRect = new RectFloat(left + mShadowRadius - mOffsetX,
                top + mShadowRadius - mOffsetY, right - mShadowRadius - mOffsetX,
                bottom - mShadowRadius - mOffsetY);
    }

    private void onDraw(Canvas canvas) {
        if (mBgColor != null) {
            if (mBgColor.length == 1) {
                Color color = new Color(mBgColor[0]);
                mBgPaint.setColor(color);
            } else {
                Color[] bgColors = new Color[mBgColor.length];
                for (int ii = 0; ii < mBgColor.length; ii++) {
                    Color color1 = new Color(mBgColor[ii]);
                    bgColors[ii] = color1;
                }

                // 重点在这里
                Point[] pointArrays = new Point[]{new Point(mRect.left, mRect.getHeight() / 2),
                        new Point(mRect.right, mRect.getHeight() / 2)};
                LinearShader shader = new LinearShader(pointArrays, new float[]{mRect.right, mRect.getHeight() / 2},
                        bgColors, Shader.TileMode.CLAMP_TILEMODE);
                mBgPaint.setShader(shader, Paint.ShaderType.LINEAR_SHADER);
            }
        }

        if (mShape == SHAPE_ROUND) {
            canvas.drawRoundRect(mRect, mShapeRadius, mShapeRadius, mShadowPaint);
            canvas.drawRoundRect(mRect, mShapeRadius, mShapeRadius, mBgPaint);
        } else {
            canvas.drawCircle(mRect.getHorizontalCenter(), mRect.getVerticalCenter(),
                    Math.min(mRect.getWidth(), mRect.getHeight()) / 2, mShadowPaint);
            canvas.drawCircle(mRect.getHorizontalCenter(), mRect.getVerticalCenter(),
                    Math.min(mRect.getWidth(), mRect.getHeight()) / 2, mBgPaint);
        }
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        super.drawToCanvas(canvas);
        onDraw(canvas);
    }

    @Override
    public void setAlpha(int alpha) {
        mShadowPaint.setAlpha(alpha);
    }

    /**
     * 颜色
     *
     * @param colorFilter 颜色
     */
    public void setColorFilter(ColorFilter colorFilter) {
        mShadowPaint.setColorFilter(colorFilter);
    }

    public int getOpacity() {
        return 0;
    }

    /**
     * 阴影图
     *
     * @param component 视图
     * @param drawable 背景
     */
    public static void setShadowDrawable(Component component, Element drawable) {
        component.setBackground(drawable);
    }

    /**
     * 为指定component添加阴影
     *
     * @param component 目标component
     * @param shapeRadius component的圆角
     * @param shadowColor 阴影的颜色
     * @param shadowRadius 阴影的宽度
     * @param offsetX 阴影水平方向的偏移量
     * @param offsetY 阴影垂直方向的偏移量
     */
    public static void setShadowDrawable(Component component, int shapeRadius, int shadowColor,
                                         int shadowRadius, int offsetX, int offsetY) {
        ShadowDrawable drawable = new Builder()
                .setShapeRadius(shapeRadius)
                .setShadowColor(shadowColor)
                .setShadowRadius(shadowRadius)
                .setOffsetX(offsetX)
                .setOffsetY(offsetY)
                .builder();
        component.addDrawTask(new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                drawable.setBounds(0, 0, component.getWidth(), component.getHeight());
                drawable.drawToCanvas(canvas);
            }
        }, Component.DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
        component.setBackground(drawable);
    }

    /**
     * 为指定component设置带阴影的背景
     *
     * @param component 目标component
     * @param bgColor component背景色
     * @param shapeRadius component的圆角
     * @param shadowColor 阴影的颜色
     * @param shadowRadius 阴影的宽度
     * @param offsetX 阴影水平方向的偏移量
     * @param offsetY 阴影垂直方向的偏移量
     */
    public static void setShadowDrawable(Component component, int bgColor, int shapeRadius,
                                         int shadowColor, int shadowRadius, int offsetX, int offsetY) {
        ShadowDrawable drawable = new Builder()
                .setBgColor(bgColor)
                .setShapeRadius(shapeRadius)
                .setShadowColor(shadowColor)
                .setShadowRadius(shadowRadius)
                .setOffsetX(offsetX)
                .setOffsetY(offsetY)
                .builder();
        component.addDrawTask(new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                drawable.setBounds(0, 0, component.getWidth(), component.getHeight());
                drawable.drawToCanvas(canvas);
            }
        }, Component.DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
        component.setBackground(drawable);
    }

    /**
     * 为指定component设置指定形状并带阴影的背景
     *
     * @param component 目标视图
     * @param shape component的形状 取值可为：GradientDrawable.RECTANGLE， GradientDrawable.OVAL， GradientDrawable.RING
     * @param bgColor component背景色
     * @param shapeRadius component的圆角
     * @param shadowColor 阴影的颜色
     * @param shadowRadius 阴影的宽度
     * @param offsetX 阴影水平方向的偏移量
     * @param offsetY 阴影垂直方向的偏移量
     */
    public static void setShadowDrawable(Component component, int shape, int bgColor,
                                         int shapeRadius, int shadowColor, int shadowRadius,
                                         int offsetX, int offsetY) {
        ShadowDrawable drawable = new Builder()
                .setShape(shape)
                .setBgColor(bgColor)
                .setShapeRadius(shapeRadius)
                .setShadowColor(shadowColor)
                .setShadowRadius(shadowRadius)
                .setOffsetX(offsetX)
                .setOffsetY(offsetY)
                .builder();
        component.addDrawTask(new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                drawable.setBounds(0, 0, component.getWidth(), component.getHeight());
                drawable.drawToCanvas(canvas);
            }
        }, Component.DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
        component.setBackground(drawable);
    }

    /**
     * 为指定视图设置带阴影的渐变背景
     *
     * @param component 视图
     * @param bgColor 颜色
     * @param shapeRadius 圆角
     * @param shadowColor 颜色
     * @param shadowRadius 阴影圆角
     * @param offsetX x
     * @param offsetY y
     */
    public static void setShadowDrawable(Component component, int[] bgColor, int shapeRadius, int shadowColor, int shadowRadius, int offsetX, int offsetY) {
        ShadowDrawable drawable = new Builder()
                .setBgColor(bgColor)
                .setShapeRadius(shapeRadius)
                .setShadowColor(shadowColor)
                .setShadowRadius(shadowRadius)
                .setOffsetX(offsetX)
                .setOffsetY(offsetY)
                .builder();
        component.addDrawTask(new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                drawable.setBounds(0, 0, component.getWidth(), component.getHeight());
                drawable.drawToCanvas(canvas);
            }
        }, Component.DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
        component.setBackground(drawable);
    }

    public static class Builder {
        private int mShape;
        private int mShapeRadius;
        private int mShadowColor;
        private int mShadowRadius;
        private int mOffsetX;
        private int mOffsetY;
        private int[] mBgColor;

        /**
         * 构造
         */
        public Builder() {
            mShape = ShadowDrawable.SHAPE_ROUND;
            mShapeRadius = 12;
            mShadowColor = Color.getIntColor("#4d000000");
            mShadowRadius = 18;
            mOffsetX = 0;
            mOffsetY = 0;
            mBgColor = new int[1];
            mBgColor[0] = Color.TRANSPARENT.getValue();
        }

        /**
         * 形状
         *
         * @param shape int类型
         * @return 本类
         */
        public Builder setShape(int shape) {
            this.mShape = shape;
            return this;
        }

        /**
         * 阴影圆角
         *
         * @param shapeRadius 圆角
         * @return 本类
         */
        public Builder setShapeRadius(int shapeRadius) {
            this.mShapeRadius = shapeRadius;
            return this;
        }

        /**
         * 阴影颜色
         *
         * @param shadowColor 颜色
         * @return 本类
         */
        public Builder setShadowColor(int shadowColor) {
            this.mShadowColor = shadowColor;
            return this;
        }

        /**
         * 阴影圆角
         *
         * @param shadowRadius 圆角
         * @return 本类this
         */
        public Builder setShadowRadius(int shadowRadius) {
            this.mShadowRadius = shadowRadius;
            return this;
        }

        /**
         * x距离
         *
         * @param offsetX x
         * @return 本类
         */
        public Builder setOffsetX(int offsetX) {
            this.mOffsetX = offsetX;
            return this;
        }

        /**
         * 距离
         *
         * @param offsetY y
         * @return 本类
         */
        public Builder setOffsetY(int offsetY) {
            this.mOffsetY = offsetY;
            return this;
        }

        /**
         * 颜色
         *
         * @param bgColor 背景颜色
         * @return 本类
         */
        public Builder setBgColor(int bgColor) {
            this.mBgColor[0] = bgColor;
            return this;
        }

        /**
         * 颜色
         *
         * @param bgColor 背景颜色数组
         * @return 本类
         */
        public Builder setBgColor(int[] bgColor) {
            this.mBgColor = bgColor;
            return this;
        }

        /**
         * 阴影
         *
         * @return 阴影
         */
        public ShadowDrawable builder() {
            return new ShadowDrawable(mShape, mBgColor, mShapeRadius, mShadowColor, mShadowRadius, mOffsetX, mOffsetY);
        }
    }
}
