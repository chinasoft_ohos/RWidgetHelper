package com.ruffian.library.widget.rounded;

import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Image.ScaleMode;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.RectFloat;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

/**
 * Round Drawable
 *
 * @author ZhongDaFeng
 */
public class RoundDrawable extends ShapeElement implements Component.DrawTask {
    private PixelMap mBitmap;
    private final int mBitmapWidth;
    private final int mBitmapHeight;
    private final RectFloat mBitmapRect = new RectFloat();

    private final Paint mBitmapPaint;
    private final Paint mBorderPaint;

    private final RectFloat mBorderRect = new RectFloat();
    private final RectFloat mDrawableRect = new RectFloat();

    private Path mPath = new Path();
    private RectFloat mRectF = new RectFloat();
    private final RectFloat mBounds = new RectFloat();
    private final RectFloat mBoundsFinal = new RectFloat();

    private boolean isRebuildShader = true;
    private final Matrix mShaderMatrix = new Matrix();
    private ScaleMode mScaleType = ScaleMode.ZOOM_CENTER;

    // 圆角
    private float mCorner = -1;
    private float mCornerTopLeft = 0;
    private float mCornerTopRight = 0;
    private float mCornerBottomLeft = 0;
    private float mCornerBottomRight = 0;
    private float mCornerRadii[] = new float[8];

    // 边框
    private float mBorderWidth = 0;
    private Color mBorderColor = Color.BLACK;

    // 是否圆形
    private boolean isCircle = true;
    private float mWidthHeight = 2;

    /**
     * 构造
     *
     * @param bitmap 图片
     */
    public RoundDrawable(PixelMap bitmap) {
        mBitmap = bitmap;
        mBitmapWidth = bitmap.getImageInfo().size.width;
        mBitmapHeight = bitmap.getImageInfo().size.height;
        mBitmapRect.fuse(0, 0, mBitmapWidth, mBitmapHeight);

        mBitmapPaint = new Paint();
        mBitmapPaint.setStyle(Paint.Style.FILL_STYLE);
        mBitmapPaint.setAntiAlias(true);

        mBorderPaint = new Paint();
        mBorderPaint.setStyle(Paint.Style.STROKE_STYLE);
        mBorderPaint.setAntiAlias(true);
        updateBorder();
    }

    @Override
    public void setAlpha(int alpha) {
        mBitmapPaint.setAlpha(alpha);
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        super.drawToCanvas(canvas);
    }

    private void updateConner() {
        if (mCorner >= 0) {
            for (int i = 0; i < mCornerRadii.length; i++) {
                mCornerRadii[i] = mCorner;
            }
            return;
        }

        if (mCorner < 0) {
            mCornerRadii[0] = mCornerTopLeft;
            mCornerRadii[1] = mCornerTopLeft;
            mCornerRadii[2] = mCornerTopRight;
            mCornerRadii[3] = mCornerTopRight;
            mCornerRadii[4] = mCornerBottomRight;
            mCornerRadii[5] = mCornerBottomRight;
            mCornerRadii[6] = mCornerBottomLeft;
            mCornerRadii[7] = mCornerBottomLeft;
        }
    }

    // 更新Drawable路径
    private void updateDrawablePath() {
        mPath.reset(); // must重置
        mPath.addRoundRect(mDrawableRect, mCornerRadii, Path.Direction.COUNTER_CLOCK_WISE);
    }

    // 更新边框路径
    private void updateBorderPath() {
        mPath.reset(); // must重置
        mPath.addRoundRect(mBorderRect, mCornerRadii, Path.Direction.COUNTER_CLOCK_WISE);
    }

    // 根据ScaleType更新ShaderMatrix此函数涉及更新的属性：mBorderWidth || mScaleType || isCircle
    private void updateShaderMatrix() {
        float scale;
        float dx = 0, dy = 0;
        float height, width;
        float half = mBorderWidth / mWidthHeight;
        mBounds.fuse(mBoundsFinal);
        switch (mScaleType) {
            case INSIDE:
                if (mBitmapWidth <= mBounds.getWidth() && mBitmapHeight <= mBounds.getHeight()) {
                    scale = 1.0f;
                    height = mBitmapHeight;
                    width = mBitmapWidth;
                } else {
                    scale = Math.min(mBounds.getWidth() / (float) mBitmapWidth, mBounds.getHeight() / (float) mBitmapHeight);
                    if (mBounds.getHeight() < mBounds.getWidth()) { // 高<宽
                        height = mBounds.getHeight();
                        width = mBitmapWidth * scale;
                    } else if (mBounds.getHeight() > mBounds.getWidth()) { // 宽<高
                        height = mBitmapHeight * scale;
                        width = mBounds.getWidth();
                    } else { // 宽=高
                        height = mBitmapHeight * scale;
                        width = mBitmapWidth * scale;
                    }
                }

                // X,Y偏移
                dx = (int) ((mBounds.getWidth() - mBitmapWidth * scale) * 0.5f + 0.5f);
                dy = (int) ((mBounds.getHeight() - mBitmapHeight * scale) * 0.5f + 0.5f);

                mRectF = new RectFloat(dx, dy, width + dx, height + dy);
                mRectF.fuse(isCircle ? mBorderWidth : half, isCircle ? mBorderWidth : half); // 非圆 1/2兼容圆角
                mShaderMatrix.reset();
                mShaderMatrix.setScale(scale, scale);
                mShaderMatrix.postTranslate(dx, dy);
                break;
            case CENTER:

                height = Math.min(mBounds.getHeight(), mBitmapRect.getHeight());
                width = Math.min(mBounds.getWidth(), mBitmapRect.getWidth());

                // 裁剪或者Margin（如果component大，则 margin Bitmap，如果component小则裁剪Bitmap）
                float cutOrMarginH = mBounds.getHeight() - mBitmapRect.getHeight();
                float cutOrMarginW = mBounds.getWidth() - mBitmapRect.getWidth();
                float halfH = cutOrMarginH / 2f, halfW = cutOrMarginW / 2f;
                float top = 0 < halfH ? halfH : 0;
                float left = 0 < halfW ? halfW : 0;
                dx = halfW;
                dy = halfH;

                mRectF = new RectFloat(left, top, left + width, top + height);
                mRectF.fuse(isCircle ? mBorderWidth : half, isCircle ? mBorderWidth : half); // 非圆 1/2兼容圆角
                mShaderMatrix.reset();
                mShaderMatrix.postTranslate((int) (dx + 0.5f) + half, (int) (dy + 0.5f) + half);
                break;
            case CLIP_CENTER:
                mRectF.fuse(mBounds);
                mRectF.fuse(isCircle ? mBorderWidth : half, isCircle ? mBorderWidth : half); // 非圆 1/2兼容圆角
                if (mBitmapWidth * mRectF.getHeight() > mRectF.getWidth() * mBitmapHeight) {
                    scale = mRectF.getHeight() / (float) mBitmapHeight;
                    dx = (mRectF.getWidth() - mBitmapWidth * scale) * 0.5f;
                } else {
                    scale = mRectF.getWidth() / (float) mBitmapWidth;
                    dy = (mRectF.getHeight() - mBitmapHeight * scale) * 0.5f;
                }
                mShaderMatrix.reset();
                mShaderMatrix.setScale(scale, scale);
                mShaderMatrix.postTranslate((int) (dx + 0.5f) + half, (int) (dy + 0.5f) + half);
                break;
            default:
            case ZOOM_CENTER:
            case ZOOM_END:
            case ZOOM_START:
                mBounds.fuse(isCircle ? mBorderWidth : half, isCircle ? mBorderWidth : half); // 非圆 1/2兼容圆角
                mRectF.fuse(mBitmapRect);
                mShaderMatrix.setRectToRect(mBitmapRect, mBounds, scaleTypeToScaleToFit(mScaleType));
                mShaderMatrix.mapRect(mRectF);
                mShaderMatrix.setRectToRect(mBitmapRect, mRectF, Matrix.ScaleToFit.FILL);
                break;
            case STRETCH:
                mBounds.fuse(isCircle ? mBorderWidth : half, isCircle ? mBorderWidth : half); // 非圆 1/2兼容圆角
                mRectF.fuse(mBounds);
                mShaderMatrix.reset();
                mShaderMatrix.setRectToRect(mBitmapRect, mRectF, Matrix.ScaleToFit.FILL);
                break;
        }
        if (isCircle) {
            mBorderRect.fuse(mRectF.left - half, mRectF.top - half, mRectF.right + half, mRectF.bottom + half); // 还原
        } else {
            mBorderRect.fuse(mBoundsFinal);
            mBorderRect.fuse(half, half);
        }
        mDrawableRect.fuse(mRectF);
        isRebuildShader = true;
    }

    /**
     * 更新边框
     */
    private void updateBorder() {
        mBorderPaint.setColor(mBorderColor);
        mBorderPaint.setStrokeWidth(mBorderWidth);
    }

    public PixelMap getBitmap() {
        return mBitmap;
    }

    /**
     * 统一设置参数
     *
     * @param scaleType img类型
     * @param borderWidth 宽度
     * @param borderColor 颜色
     * @param isCircle 是否
     * @param corner 圆角
     * @param topLeft 左上
     * @param topRight 右上
     * @param bottomLeft 下左
     * @param bottomRight 下右
     */
    public void setParams(Image.ScaleMode scaleType, float borderWidth, Color borderColor, boolean isCircle, float corner, float topLeft, float topRight, float bottomLeft, float bottomRight) {
        // scaleType
        if (scaleType == null) {
            scaleType = ScaleMode.ZOOM_CENTER;
        }
        if (mScaleType != scaleType) {
            mScaleType = scaleType;
        }

        // borderWidth
        mBorderWidth = borderWidth;

        // borderColor
        mBorderColor = borderColor;
        updateBorder();

        // isCircle
        this.isCircle = isCircle;

        // corner
        mCorner = corner;
        mCornerTopLeft = topLeft;
        mCornerTopRight = topRight;
        mCornerBottomLeft = bottomLeft;
        mCornerBottomRight = bottomRight;
        updateConner();

        // update
        updateShaderMatrix(); // 更新变化矩阵
    }

    /**
     * 更新变化矩阵
     *
     * @param scaleType 类型
     * @return 返回图的type类型
     */
    public RoundDrawable setScaleType(Image.ScaleMode scaleType) {
        if (scaleType == null) {
            scaleType = ScaleMode.ZOOM_CENTER;
        }
        if (mScaleType != scaleType) {
            mScaleType = scaleType;
            updateShaderMatrix();
        }
        return this;
    }

    /**
     * 设置圆
     *
     * @param isCircle true是，反之
     * @return 本类
     */
    public RoundDrawable setCircle(boolean isCircle) {
        this.isCircle = isCircle;
        updateShaderMatrix(); // 更新变化矩阵
        return this;
    }

    /**
     * 更新变化矩阵
     *
     * @param borderWidth 宽度
     * @return 宽度
     */
    public RoundDrawable setBorderWidth(float borderWidth) {
        mBorderWidth = borderWidth;
        updateBorder();
        updateShaderMatrix();
        return this;
    }

    /**
     * 颜色
     *
     * @param borderColor 颜色
     * @return 颜色
     */
    public RoundDrawable setBorderColor(Color borderColor) {
        mBorderColor = borderColor;
        updateBorder();
        return this;
    }

    /**
     * 位置
     *
     * @param corner float
     * @param topLeft 左
     * @param topRight 右
     * @param bottomLeft 下左
     * @param bottomRight 下右
     * @return 圆角位置
     */
    public RoundDrawable setConner(float corner, float topLeft, float topRight, float bottomLeft, float bottomRight) {
        mCorner = corner;
        mCornerTopLeft = topLeft;
        mCornerTopRight = topRight;
        mCornerBottomLeft = bottomLeft;
        mCornerBottomRight = bottomRight;
        updateConner();
        return this;
    }

    /**
     * 图片
     *
     * @param bitmap 图
     * @return 图
     */
    public static RoundDrawable fromBitmap(PixelMap bitmap) {
        if (bitmap != null) {
            return new RoundDrawable(bitmap);
        } else {
            return null;
        }
    }

    /**
     * 绘制的位图
     *
     * @param img 图
     * @return 图
     */
    public static Element fromDrawable(Element img) {
        if (img != null) {
            PixelMap bm = drawableToBitmap(img);
            if (bm != null) {
                return new RoundDrawable(bm);
            }
        }
        return img;
    }

    /**
     * 图
     *
     * @param element 背景
     * @return 图
     */
    public static PixelMap drawableToBitmap(Element element) {
        if (element instanceof PixelMapElement) {
            return ((PixelMapElement) element).getPixelMap();
        }

        PixelMap bitmap;
        int width = Math.max(element.getWidth(), 2);
        int height = Math.max(element.getHeight(), 2);
        try {
            PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
            options.pixelFormat = PixelFormat.ARGB_8888;
            options.size = new Size(width, height);
            bitmap = PixelMap.create(options);
            Texture texture = new Texture(bitmap);
            Canvas canvas = new Canvas(texture);
            element.setBounds(0, 0, canvas.getLocalClipBounds().getWidth(), canvas.getLocalClipBounds().getHeight());
            element.drawToCanvas(canvas);
        } catch (Throwable e) {
            e.printStackTrace();
            bitmap = null;
        }

        return bitmap;
    }

    private static Matrix.ScaleToFit scaleTypeToScaleToFit(ScaleMode st) {
        switch (st) {
            case STRETCH:
                return Matrix.ScaleToFit.FILL;
            case ZOOM_START:
                return Matrix.ScaleToFit.START;
            case ZOOM_END:
                return Matrix.ScaleToFit.END;
            default:
                return Matrix.ScaleToFit.CENTER;
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (isRebuildShader) {
            PixelMapHolder holder = new PixelMapHolder(mBitmap);
            PixelMapShader bitmapShader = new PixelMapShader(holder,
                    Shader.TileMode.CLAMP_TILEMODE, Shader.TileMode.CLAMP_TILEMODE);
            bitmapShader.setShaderMatrix(mShaderMatrix); // Shader.TileMode.CLAMP
            mBitmapPaint.setShader(bitmapShader, Paint.ShaderType.PIXELMAP_SHADER);
            isRebuildShader = false;
        }

        if (isCircle) {
            // mDrawableRect 宽高是实际显示图片的宽高，类似于 marginTop 了  mDrawableRect.left
            float cx = mDrawableRect.getWidth() / mWidthHeight + mDrawableRect.left;
            float cy = mDrawableRect.getHeight() / mWidthHeight + mDrawableRect.top;
            float radiusX = mDrawableRect.getWidth() / mWidthHeight;
            float radiusY = mDrawableRect.getHeight() / mWidthHeight;
            float radiusDrawable = Math.min(radiusX, radiusY);
            float radiusBitmap = Math.min(mBitmapHeight, mBitmapWidth);
            float radius = Math.min(radiusBitmap, radiusDrawable);
            canvas.drawCircle(cx, cy, radius, mBitmapPaint);

            if (mBorderWidth > 0) {
                cx = mBorderRect.getWidth() / mWidthHeight + mBorderRect.left;
                cy = mBorderRect.getHeight() / mWidthHeight + mBorderRect.top;
                radiusX = mBorderRect.getWidth() / mWidthHeight;
                radiusY = mBorderRect.getHeight() / mWidthHeight;
                radiusDrawable = Math.min(radiusX, radiusY);
                radiusBitmap = Math.min(mBitmapHeight, mBitmapWidth);
                radius = Math.min(radiusBitmap, radiusDrawable);
                canvas.drawCircle(cx, cy, radius, mBorderPaint);
            }
        } else {
            updateDrawablePath();
            canvas.drawPath(mPath, mBitmapPaint);
            if (mBorderWidth > 0) {
                updateBorderPath();
                canvas.drawPath(mPath, mBorderPaint);
            }
        }
    }
}
