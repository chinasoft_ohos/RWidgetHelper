package com.ruffian.library.widget.component;

import com.ruffian.library.widget.helper.RBaseHelper;
import com.ruffian.library.widget.iface.RHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * RView
 *
 * @author ZhongDaFeng
 */
public class RComponent extends Component implements RHelper<RBaseHelper> {
    private RBaseHelper mHelper;

    /**
     * 构造
     *
     * @param context 上下文
     */
    public RComponent(Context context) {
        this(context, null);
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrs 属性集
     */
    public RComponent(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrs 属性集
     * @param defStyleAttr 默认
     */
    public RComponent(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mHelper = new RBaseHelper(context, this, attrs);
    }

    @Override
    public RBaseHelper getHelper() {
        return mHelper;
    }
}
