package com.ruffian.library.widget.iface;

import com.ruffian.library.widget.helper.RBaseHelper;

/**
 * Helper
 *
 * @author ZhongDaFeng
 */
public interface RHelper<T extends RBaseHelper> {
    /**
     * 获取帮助
     *
     * @return 任意类型
     */
    T getHelper();
}
