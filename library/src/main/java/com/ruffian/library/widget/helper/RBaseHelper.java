package com.ruffian.library.widget.helper;

import static ohos.agp.components.ComponentState.*;

import com.ruffian.library.widget.clip.ClipHelper;
import com.ruffian.library.widget.clip.ClipPathManager;
import com.ruffian.library.widget.clip.IClip;
import com.ruffian.library.widget.shadow.ShadowDrawable;
import com.ruffian.library.widget.utils.AttrUtils;
import com.ruffian.library.widget.utils.DensityUtils;
import com.ruffian.library.widget.utils.Log;
import com.ruffian.library.widget.utils.LogUtil;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * BaseHelper
 *
 * @author ZhongDaFeng
 */
public class RBaseHelper<T extends Component> implements IClip, Component.LayoutRefreshedListener,
        Component.DrawTask, Component.TouchEventListener {
    /**
     * 渐变方向
     */
    private static final String TOP_BOTTOM = "TOP_BOTTOM";
    private static final String TR_BL = "TR_BL";
    private static final String RIGHT_LEFT = "RIGHT_LEFT";
    private static final String BR_TL = "BR_TL";
    private static final String BOTTOM_TOP = "BOTTOM_TOP";
    private static final String BL_TR = "BL_TR";
    private static final String LEFT_RIGHT = "LEFT_RIGHT";
    private static final String TL_BR = "TL_BR";
    /**
     * 背景类型{1:单一颜色值 2:颜色数组 3:图片}
     */
    protected int BG_TYPE_COLOR = 1;
    protected int BG_TYPE_COLOR_ARRAY = 2;
    protected int BG_TYPE_IMG = 3;
    protected Context mContext;
    protected T mComponent;
    /**
     * 边框颜色
     */
    private String mBorderHueNormal = "border_color_normal";
    private String mBorderHuePressed = "border_color_pressed";
    private String mBorderHueUnable = "border_color_unable";
    private String mBorderHueChecked = "border_color_checked";
    private String mBorderHueSelected = "border_color_selected";
    private String mShadeType = "gradient_type";
    private String mShadeRadius = "gradient_radius";
    private String mShadeCenterX = "gradient_centerX";
    private String mShadeCenterY = "gradient_centerY";
    private String mEnabled = "enabled";
    private String strBgType = "";
    /**
     * 边框宽度
     */
    private String mBorderWideNormal = "border_width_normal";
    private String mBorderWidePressed = "border_width_pressed";
    private String mBorderWideUnable = "border_width_unable";
    private String mBorderWideChecked = "border_width_checked";
    private String mBorderWideSelected = "border_width_selected";
    /**
     * 自定义背景key
     */
    private String mBackgroundCoNormal = "background_normal";
    private String mBackgroundCoPressed = "background_pressed";
    private String mBackgroundCoUnable = "background_unable";
    private String mBackgroundCoChecked = "background_checked";
    private String mBackgroundCoSelected = "background_selected";
    private String mGradient = "gradient_orientation";
    private String mCircular = "corner_radius";
    private String mCircularTopLeft = "corner_radius_top_left";
    private String mCircularTopRight = "corner_radius_top_right";
    private String mCircularBottomLeft = "corner_radius_bottom_left";
    private String mCircularBottomRight = "corner_radius_bottom_right";

    // corner
    private float mCornerRadius;

    private float mCornerRadiusTopLeft;
    private float mCornerRadiusTopRight;
    private float mCornerRadiusBottomLeft;
    private float mCornerRadiusBottomRight;

    // BorderWidth
    private float mBorderDashWidth = 0;
    private float mBorderDashGap = 0;
    private int mBorderWidthNormal = 0;
    private int mBorderWidthPressed = 0;
    private int mBorderWidthUnable = 0;
    private int mBorderWidthChecked = 0;
    private int mBorderWidthSelected = 0;

    // BorderColor
    private Color mBorderColorNormal;
    private Color mBorderColorPressed;
    private Color mBorderColorUnable;
    private Color mBorderColorChecked;
    private Color mBorderColorSelected;
    private Color mBackgroundColorNormal;
    private Color mBackgroundColorPressed;
    private Color mBackgroundColorUnable;
    private Color mBackgroundColorChecked;
    private Color mBackgroundColorSelected;
    private Color[] mBackgroundColorNormalArray;
    private Color[] mBackgroundColorPressedArray;
    private Color[] mBackgroundColorUnableArray;
    private Color[] mBackgroundColorCheckedArray;
    private Color[] mBackgroundColorSelectedArray;
    private ShapeElement mBackgroundNormal;
    private ShapeElement mBackgroundPressed;
    private ShapeElement mBackgroundUnable;
    private ShapeElement mBackgroundChecked;
    private ShapeElement mBackgroundSelected;
    private Element mBackgroundNormalBmp;
    private Element mBackgroundPressedBmp;
    private Element mBackgroundUnableBmp;
    private Element mBackgroundCheckedBmp;
    private Element mBackgroundSelectedBmp;

    // Gradient
    private int mGradientType = 0;
    private float mGradientRadius;
    private float mGradientCenterX;
    private float mGradientCenterY;
    private ShapeElement.Orientation mGradientOrientation = ShapeElement.Orientation.BOTTOM_TO_TOP;
    private int mShadowDx;
    private int mShadowDy;
    private Color mShadowColor;
    private int mShadowRadius;

    private boolean mIsEnabled = true;

    // ripple
    private boolean isUseRipple;
    private Color mRippleColor;
    private Element mRippleMaskDrawable;
    private Element mComponentBackground; // 控件默认背景Drawable
    private Element mBackgroundDrawable;

    private int[][] states = new int[6][];
    private StateElement mStateBackground;
    private float[] mBorderRadii = new float[8];

    /**
     * Cache the touch slop from the context that created the component.
     */
    private int mTouchSlop = 0;
    /**
     * 是否设置对应的属性
     */
    private boolean isHasPressedBgColor = false;
    private boolean isHasPressedBgBmp = false;
    private boolean isHasUnableBgColor = false;
    private boolean isHasUnableBgBmp = false;
    private boolean isHasCheckedBgColor = false;
    private boolean isHasSelectedBgColor = false;
    private boolean isHasCheckedBgBmp = false;
    private boolean isHasSelectedBgBmp = false;
    private boolean isHasPressedBorderColor = false;
    private boolean isHasUnableBorderColor = false;
    private boolean isHasCheckedBorderColor = false;
    private boolean isHasSelectedBorderColor = false;
    private boolean isHasPressedBorderWidth = false;
    private boolean isHasUnableBorderWidth = false;
    private boolean isHasCheckedBorderWidth = false;
    private boolean isHasSelectedBorderWidth = false;
    private StateElement emptyStateListDrawable = new StateElement(); // EmptyStateListDrawable
    protected ClipHelper mClipHelper = new ClipHelper(); // ClipHelper
    private boolean isClipLayout = false; // clipLayout
    private MmiPoint pointerPosition; // 水波纹相关属性
    private int sy;
    private float sx;
    AnimatorValue animatorValue = new AnimatorValue();
    private float zj;
    private Paint paint;

    /**
     * 构造
     *
     * @param context 上下文
     * @param component component
     * @param attrs 属性集
     */
    public RBaseHelper(Context context, T component, AttrSet attrs) {
        mComponent = component;
        mContext = context;
        mComponent.addDrawTask(this);
        mComponent.setTouchEventListener(this);
        mComponent.setLayoutRefreshedListener(this);
        initAttributeSet(context, attrs);
        paint = new Paint();
        paint.setColor(mRippleColor);

        paint.setAlpha(0.2f);
        paint.setStyle(Paint.Style.FILL_STYLE);
        animatorValue.setDuration(1000);
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
    }

    /**
     * 初始化控件属性
     *
     * @param context 上下文
     * @param attrs
     */
    private void initAttributeSet(Context context, AttrSet attrs) {
        if (context == null || attrs == null) {
            setup();
            return;
        }
        mCornerRadius = AttrUtils.getInt(attrs, mCircular, -1);

        mCornerRadiusTopLeft = AttrUtils.getInt(attrs, mCircularTopLeft, 0);
        mCornerRadiusTopRight = AttrUtils.getInt(attrs, mCircularTopRight, 0);
        mCornerRadiusBottomLeft = AttrUtils.getInt(attrs, mCircularBottomLeft, 0);
        mCornerRadiusBottomRight = AttrUtils.getInt(attrs, mCircularBottomRight, 0);

        // border
        mBorderWidthNormal = AttrUtils.getInt(attrs, mBorderWideNormal, 0);
        mBorderWidthPressed = AttrUtils.getInt(attrs, mBorderWidePressed, 0);
        mBorderWidthUnable = AttrUtils.getInt(attrs, mBorderWideUnable, 0);
        mBorderWidthChecked = AttrUtils.getInt(attrs, mBorderWideChecked, 0);
        mBorderWidthSelected = AttrUtils.getInt(attrs, mBorderWideSelected, 0);
        mBorderDashGap = AttrUtils.getFloat(attrs, "border_dash_gap", 0);
        mBorderDashWidth = AttrUtils.getFloat(attrs, "border_dash_width", 0);
        mBorderColorNormal = getStringColor(attrs, mBorderHueNormal, null); // border 颜色
        mBorderColorPressed = getStringColor(attrs, mBorderHuePressed, null);
        mBorderColorUnable = getStringColor(attrs, mBorderHueUnable, null);
        mBorderColorChecked = getStringColor(attrs, mBorderHueChecked, null);
        mBorderColorSelected = getStringColor(attrs, mBorderHueSelected, null);
        strBgType = AttrUtils.getXmlManyValue(attrs, "bgType", "color");

        Object[] bgInfoNormal;

        bgInfoNormal = getBackgroundInfo(attrs, mBackgroundCoNormal);

        mBackgroundColorNormal = (Color) bgInfoNormal[1];
        mBackgroundColorNormalArray = (Color[]) bgInfoNormal[2];
        mBackgroundNormalBmp = (Element) bgInfoNormal[3];
        Log.i("mono1" + mBackgroundNormalBmp);
        Object[] bgInfoPressed = getBackgroundInfo(attrs, mBackgroundCoPressed); // pressed
        mBackgroundColorPressed = (Color) bgInfoPressed[1];
        mBackgroundColorPressedArray = (Color[]) bgInfoPressed[2];
        mBackgroundPressedBmp = (Element) bgInfoPressed[3];
        Object[] bgInfoUnable = getBackgroundInfo(attrs, mBackgroundCoUnable); // unable
        mBackgroundColorUnable = (Color) bgInfoUnable[1];
        mBackgroundColorUnableArray = (Color[]) bgInfoUnable[2];
        mBackgroundUnableBmp = (Element) bgInfoUnable[3];
        Object[] bgInfoChecked = getBackgroundInfo(attrs, mBackgroundCoChecked); // checked
        mBackgroundColorChecked = (Color) bgInfoChecked[1];
        mBackgroundColorCheckedArray = (Color[]) bgInfoChecked[2];
        mBackgroundCheckedBmp = (Element) bgInfoChecked[3];
        Object[] bgInfoSelected = getBackgroundInfo(attrs, mBackgroundCoSelected); // selected
        mBackgroundColorSelected = (Color) bgInfoSelected[1];
        mBackgroundColorSelectedArray = (Color[]) bgInfoSelected[2];
        mBackgroundSelectedBmp = (Element) bgInfoSelected[3];
        mGradientType = AttrUtils.getInt(attrs, mShadeType, 0); // gradient
        mGradientOrientation = getGradientOrientation(attrs);
        mGradientRadius = AttrUtils.getFloat(attrs, mShadeRadius, -1);
        mGradientCenterX = AttrUtils.getFloat(attrs, mShadeCenterX, 0.5f);
        mGradientCenterY = AttrUtils.getFloat(attrs, mShadeCenterY, 0.5f);
        mIsEnabled = getBool(attrs, mEnabled, "true");
        try {
            isUseRipple = getBool(attrs, "ripple", "false"); // Ripple
            mRippleColor = getStringColor(attrs, "ripple_color", null);
            mRippleMaskDrawable = AttrUtils.getElement(attrs, "ripple_mask", null);
        } catch (Exception e) {
            LogUtil.i("ExceptionMessage=====" + e.getMessage());
        }
        mShadowDx = AttrUtils.getInt(attrs, "shadow_dx", 0); // shadow
        mShadowDy = AttrUtils.getInt(attrs, "shadow_dy", 0);
        mShadowColor = getStringColor(attrs, "shadow_color", null);
        mShadowRadius = AttrUtils.getInt(attrs, "shadow_radius", -1);
        isClipLayout = getBool(attrs, "clip_layout", "false"); // clip
        isHasPressedBgColor = mBackgroundColorPressed != null || mBackgroundColorPressedArray != null;
        isHasUnableBgColor = mBackgroundColorUnable != null || mBackgroundColorUnableArray != null;
        isHasCheckedBgColor = mBackgroundColorChecked != null || mBackgroundColorCheckedArray != null;
        isHasSelectedBgColor = mBackgroundColorSelected != null || mBackgroundColorSelectedArray != null;
        isHasPressedBgBmp = mBackgroundPressedBmp != null;
        isHasUnableBgBmp = mBackgroundUnableBmp != null;
        isHasCheckedBgBmp = mBackgroundCheckedBmp != null;
        isHasSelectedBgBmp = mBackgroundSelectedBmp != null;
        LogUtil.i("true or false====" + isHasPressedBorderColor);
        isHasPressedBorderColor = mBorderColorPressed != null;
        LogUtil.i("true or false=====" + isHasPressedBorderColor);
        isHasUnableBorderColor = mBorderColorUnable != null;
        isHasCheckedBorderColor = mBorderColorChecked != null;
        isHasSelectedBorderColor = mBorderColorSelected != null;
        isHasPressedBorderWidth = mBorderWidthPressed != 0;
        isHasUnableBorderWidth = mBorderWidthUnable != 0;
        isHasCheckedBorderWidth = mBorderWidthChecked != 0;
        isHasSelectedBorderWidth = mBorderWidthSelected != 0;
        Log.i("------------------" + mBackgroundNormalBmp);
        setup();
    }

    private boolean getBool(AttrSet attrs, String enabled, String s) {
        String bool = AttrUtils.getString(attrs, enabled, s);
        return bool.toLowerCase().equals("true");
    }

    private Color getStringColor(AttrSet attrs, String colorKey, String s) {
        String color = AttrUtils.getString(attrs, colorKey, s);
        if (color == null) {
            return Color.TRANSPARENT;
        }
        return new Color(Color.getIntColor(color));
    }

    /**
     * 设置
     */
    private void setup() {
        boolean isEnabled = mComponent.isEnabled();
        if (isEnabled) {
            mComponent.setEnabled(mIsEnabled);
        }

        mBackgroundNormal = new ShapeElement();
        mBackgroundPressed = new ShapeElement();
        mBackgroundUnable = new ShapeElement();
        mBackgroundChecked = new ShapeElement();
        mBackgroundSelected = new ShapeElement();
        mComponentBackground = mComponent.getBackgroundElement();
        mStateBackground = new StateElement();

        /**
         * 设置背景默认值
         * 备注:当存在 Checked 状态并且没有设置 Pressed 时，Pressed = Checked 更符合常规UI
         */
        if (!isHasPressedBgColor) {
            mBackgroundColorPressed = isHasCheckedBgColor ? mBackgroundColorChecked : mBackgroundColorNormal;
            mBackgroundColorPressedArray = isHasCheckedBgColor ?
                    mBackgroundColorCheckedArray : mBackgroundColorNormalArray;
        }
        if (!isHasPressedBgBmp) {
            mBackgroundPressedBmp = isHasCheckedBgBmp ? mBackgroundCheckedBmp : mBackgroundNormalBmp;
        }
        if (!isHasUnableBgColor) {
            mBackgroundColorUnable = mBackgroundColorNormal;
            mBackgroundColorUnableArray = mBackgroundColorNormalArray;
        }
        if (!isHasUnableBgBmp) {
            mBackgroundUnableBmp = mBackgroundNormalBmp;
        }
        if (!isHasCheckedBgColor) {
            mBackgroundColorChecked = mBackgroundColorNormal;
            mBackgroundColorCheckedArray = mBackgroundColorNormalArray;
        }
        if (!isHasSelectedBgColor) {
            mBackgroundColorSelected = mBackgroundColorNormal;
            mBackgroundColorSelectedArray = mBackgroundColorNormalArray;
        }
        if (!isHasCheckedBgBmp) {
            mBackgroundCheckedBmp = mBackgroundNormalBmp;
        }
        if (!isHasSelectedBgBmp) {
            mBackgroundSelectedBmp = mBackgroundNormalBmp;
        }

        /**
         * 设置背景颜色（包含渐变）
         */
        if (mBackgroundColorNormalArray != null && mBackgroundColorNormalArray.length > 0) {
            mBackgroundNormal = setColors(mBackgroundNormal, colorArrayToIntArray(mBackgroundColorNormalArray));
        } else {
            if (mBackgroundColorNormal != null) {
                mBackgroundNormal.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorNormal.getValue()));
            }
        }
        if (mBackgroundColorPressedArray != null && mBackgroundColorPressedArray.length > 0) {
            mBackgroundPressed = setColors(mBackgroundPressed, colorArrayToIntArray(mBackgroundColorPressedArray));
        } else {
            if (mBackgroundColorPressed != null) {
                mBackgroundPressed.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorPressed.getValue()));
            }
        }
        if (mBackgroundColorUnableArray != null && mBackgroundColorUnableArray.length > 0) {
            mBackgroundUnable = setColors(mBackgroundUnable, colorArrayToIntArray(mBackgroundColorUnableArray));
        } else {
            if (mBackgroundColorUnable != null) {
                mBackgroundUnable.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorUnable.getValue()));
            }
        }
        if (mBackgroundColorCheckedArray != null && mBackgroundColorCheckedArray.length > 0) {
            mBackgroundChecked = setColors(mBackgroundChecked, colorArrayToIntArray(mBackgroundColorCheckedArray));
        } else {
            if (mBackgroundColorChecked != null) {
                mBackgroundChecked.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorChecked.getValue()));
            }
        }
        if (mBackgroundColorSelectedArray != null && mBackgroundColorSelectedArray.length > 0) {
            mBackgroundSelected = setColors(mBackgroundSelected, colorArrayToIntArray(mBackgroundColorSelectedArray));
        } else {
            if (mBackgroundColorSelected != null) {
                mBackgroundSelected.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorSelected.getValue()));
            }
        }
        setGradient();

        // unable,focused,pressed,checked,selected,normal
        states[0] = new int[]{COMPONENT_STATE_DISABLED}; // unable
        states[1] = new int[]{COMPONENT_STATE_FOCUSED}; // focused
        states[2] = new int[]{COMPONENT_STATE_PRESSED}; // pressed
        states[3] = new int[]{COMPONENT_STATE_CHECKED}; // checked
        states[4] = new int[]{COMPONENT_STATE_SELECTED}; // selected
        states[5] = new int[]{COMPONENT_STATE_EMPTY}; // normal

        // unable,focused,pressed,checked,normal
        mStateBackground.addState(states[0], mBackgroundUnableBmp == null ? mBackgroundUnable : mBackgroundUnableBmp);
        mStateBackground.addState(states[1], mBackgroundPressedBmp == null ? mBackgroundPressed : mBackgroundPressedBmp);
        mStateBackground.addState(states[2], mBackgroundPressedBmp == null ? mBackgroundPressed : mBackgroundPressedBmp);
        mStateBackground.addState(states[3], mBackgroundCheckedBmp == null ? mBackgroundChecked : mBackgroundCheckedBmp);
        mStateBackground.addState(states[4], mBackgroundSelectedBmp == null ? mBackgroundSelected : mBackgroundSelectedBmp);
        mStateBackground.addState(states[5], mBackgroundNormalBmp == null ? mBackgroundNormal : mBackgroundNormalBmp);

        /**
         * 设置边框默认值
         */
        if (!isHasPressedBorderWidth) {
            mBorderWidthPressed = isHasCheckedBorderWidth ? mBorderWidthChecked : mBorderWidthNormal;
        }
        if (!isHasUnableBorderWidth) {
            mBorderWidthUnable = mBorderWidthNormal;
        }
        if (!isHasCheckedBorderWidth) {
            mBorderWidthChecked = mBorderWidthNormal;
        }
        if (!isHasSelectedBorderWidth) {
            mBorderWidthSelected = mBorderWidthNormal;
        }
        if (!isHasPressedBorderColor) {
            mBorderColorPressed = isHasCheckedBorderColor ? mBorderColorChecked : mBorderColorNormal;
        }
        if (!isHasUnableBorderColor) {
            mBorderColorUnable = mBorderColorNormal;
        }
        if (!isHasCheckedBorderColor) {
            mBorderColorChecked = mBorderColorNormal;
        }
        if (!isHasSelectedBorderColor) {
            mBorderColorSelected = mBorderColorNormal;
        }

        // 设置背景

        // 设置边框
        setBorder();

        // 设置圆角
        setRadiusValue();
    }

    private int[] colorArrayToIntArray(Color[] array) {
        int[] intArray = new int[0];
        if (array != null) {
            intArray = new int[array.length];
            for (int ii = 0; ii < array.length; ii++) {
                intArray[ii] = array[ii].getValue();
            }
            return intArray;
        } else {
            return intArray;
        }
    }

    /**
     * 渐变 圆角
     *
     * @return 圆角
     */
    public float getGradientRadius() {
        return mGradientRadius;
    }

    public float getGradientCenterX() {
        return mGradientCenterX;
    }

    public float getGradientCenterY() {
        return mGradientCenterY;
    }

    public int getGradientType() {
        return mGradientType;
    }

    /**
     * 构造
     *
     * @param gradientRadius 角度
     * @return 返回角度
     */
    public RBaseHelper setGradientRadius(float gradientRadius) {
        this.mGradientRadius = gradientRadius;
        setGradient();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param gradientCenterX x
     * @return 背景
     */
    public RBaseHelper setGradientCenterX(float gradientCenterX) {
        this.mGradientCenterX = gradientCenterX;
        setGradient();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param gradientCenterY y
     * @return 背景
     */
    public RBaseHelper setGradientCenterY(float gradientCenterY) {
        this.mGradientCenterY = gradientCenterY;
        setGradient();
        setBackgroundState();
        return this;
    }

    /**
     * 设置渐变样式/类型
     *
     * @param gradientType {LINEAR_GRADIENT=0, RADIAL_GRADIENT=1, SWEEP_GRADIENT=2}
     * @return 上下文
     */
    public RBaseHelper setGradientType(int gradientType) {
        if (gradientType < 0 || gradientType > 2) {
            gradientType = 0;
        }
        this.mGradientType = gradientType;
        setGradient();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param orientation 方向
     * @return 背景
     */
    public RBaseHelper setGradientOrientation(ShapeElement.Orientation orientation) {
        this.mGradientOrientation = orientation;
        setGradient();
        setBackgroundState();
        return this;
    }

    private void setGradient() {
        mBackgroundNormal.setShaderType(mGradientType);
        mBackgroundNormal.setCornerRadius(mGradientRadius);
        mBackgroundPressed.setShaderType(mGradientType);
        mBackgroundPressed.setCornerRadius(mGradientRadius);
        mBackgroundUnable.setShaderType(mGradientType);
        mBackgroundUnable.setCornerRadius(mGradientRadius);
        mBackgroundChecked.setShaderType(mGradientType);
        mBackgroundChecked.setCornerRadius(mGradientRadius);
        mBackgroundSelected.setShaderType(mGradientType);
        mBackgroundSelected.setCornerRadius(mGradientRadius);
    }

    /**
     * 背景颜色
     *
     * @param normal 默认
     * @param pressed 选中时
     * @param unable 初始
     * @param checked 选中后
     * @param selected 选中后
     * @return 上下文
     */
    public RBaseHelper setStateBackgroundColor(Color normal, Color pressed, Color unable, Color checked, Color selected) {
        mBackgroundColorNormal = normal;
        mBackgroundColorPressed = pressed;
        mBackgroundColorUnable = unable;
        mBackgroundColorChecked = checked;
        mBackgroundColorSelected = selected;
        isHasPressedBgColor = true;
        isHasUnableBgColor = true;
        isHasCheckedBgColor = true;
        isHasSelectedBgColor = true;
        mBackgroundNormal.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorNormal.getValue()));
        mBackgroundPressed.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorPressed.getValue()));
        mBackgroundUnable.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorUnable.getValue()));
        mBackgroundChecked.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorChecked.getValue()));
        mBackgroundSelected.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorSelected.getValue()));
        setBackgroundState();
        return this;
    }

    /**
     * 背景
     *
     * @param normalArray 颜色
     * @param pressedArray 颜色
     * @param unableArray 颜色
     * @param checkedArray 颜色
     * @param selectedArray 颜色
     * @return 颜色集合
     */
    public RBaseHelper setStateBackgroundColorArray(Color[] normalArray, Color[] pressedArray, Color[] unableArray, Color[] checkedArray, Color[] selectedArray) {
        if (normalArray != null) {
            mBackgroundColorNormalArray = normalArray.clone();
        }
        if (pressedArray != null) {
            mBackgroundColorPressedArray = pressedArray.clone();
        }
        if (unableArray != null) {
            mBackgroundColorUnableArray = unableArray.clone();
        }
        if (checkedArray != null) {
            mBackgroundColorCheckedArray = checkedArray.clone();
        }
        if (selectedArray != null) {
            mBackgroundColorSelectedArray = selectedArray.clone();
        }
        isHasPressedBgColor = true;
        isHasUnableBgColor = true;
        isHasCheckedBgColor = true;
        isHasSelectedBgColor = true;
        mBackgroundNormal = setColors(mBackgroundNormal, colorArrayToIntArray(mBackgroundColorNormalArray));
        mBackgroundPressed = setColors(mBackgroundPressed, colorArrayToIntArray(mBackgroundColorPressedArray));
        mBackgroundUnable = setColors(mBackgroundUnable, colorArrayToIntArray(mBackgroundColorUnableArray));
        mBackgroundChecked = setColors(mBackgroundChecked, colorArrayToIntArray(mBackgroundColorCheckedArray));
        mBackgroundSelected = setColors(mBackgroundSelected, colorArrayToIntArray(mBackgroundColorSelectedArray));
        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    /**
     * 背景
     *
     * @param normal 背景
     * @param pressed 背景
     * @param unable 背景
     * @param checked 背景
     * @param selected 背景
     * @return 背景颜色
     */
    public RBaseHelper setStateBackgroundColor(Element normal, Element pressed, Element unable, Element checked, Element selected) {
        mBackgroundNormalBmp = normal;
        mBackgroundPressedBmp = pressed;
        mBackgroundUnableBmp = unable;
        mBackgroundCheckedBmp = checked;
        mBackgroundSelectedBmp = selected;
        isHasPressedBgBmp = true;
        isHasUnableBgBmp = true;
        isHasCheckedBgBmp = true;
        isHasSelectedBgBmp = true;
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    public Color getBackgroundColorNormal() {
        return mBackgroundColorNormal;
    }

    public Color getBackgroundColorPressed() {
        return mBackgroundColorPressed;
    }

    public Color getBackgroundColorUnable() {
        return mBackgroundColorUnable;
    }

    public Color getBackgroundColorChecked() {
        return mBackgroundColorChecked;
    }

    public Color getBackgroundColorSelected() {
        return mBackgroundColorSelected;
    }

    public Color[] getBackgroundColorNormalArray() {
        if (mBackgroundColorNormalArray == null) {
            return null;
        }
        return mBackgroundColorNormalArray.clone();
    }

    public Color[] getBackgroundColorPressedArray() {
        if (mBackgroundColorPressedArray == null) {
            return null;
        }
        return mBackgroundColorPressedArray.clone();
    }

    public Color[] getBackgroundColorUnableArray() {
        if (mBackgroundColorUnableArray == null) {
            return null;
        }
        return mBackgroundColorUnableArray.clone();
    }

    public Color[] getBackgroundColorCheckedArray() {
        if (mBackgroundColorCheckedArray == null) {
            return null;
        }
        return mBackgroundColorCheckedArray.clone();
    }

    public Color[] getBackgroundColorSelectedArray() {
        if (mBackgroundColorSelectedArray == null) {
            return null;
        }
        return mBackgroundColorSelectedArray.clone();
    }

    public Element getBackgroundDrawableNormal() {
        return mBackgroundNormalBmp;
    }

    public Element getBackgroundDrawablePressed() {
        return mBackgroundPressedBmp;
    }

    public Element getBackgroundDrawableUnable() {
        return mBackgroundUnableBmp;
    }

    public Element getBackgroundDrawableChecked() {
        return mBackgroundCheckedBmp;
    }

    public Element getBackgroundDrawableSelected() {
        return mBackgroundSelectedBmp;
    }

    /**
     * 构造
     *
     * @param colorNormal 颜色
     * @return 颜色
     */
    public RBaseHelper setBackgroundColorNormal(Color colorNormal) {
        this.mBackgroundColorNormal = colorNormal;
        /**
         * 设置背景默认值
         */
        if (!isHasPressedBgColor) {
            mBackgroundColorPressed = isHasCheckedBgColor ? mBackgroundColorChecked : mBackgroundColorNormal;
            mBackgroundPressed.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorPressed.getValue()));
        }
        if (!isHasUnableBgColor) {
            mBackgroundColorUnable = mBackgroundColorNormal;
            mBackgroundUnable.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorUnable.getValue()));
        }
        if (!isHasCheckedBgColor) {
            mBackgroundColorChecked = mBackgroundColorNormal;
            mBackgroundChecked.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorChecked.getValue()));
        }
        if (!isHasSelectedBgColor) {
            mBackgroundColorSelected = mBackgroundColorNormal;
            mBackgroundSelected.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorSelected.getValue()));
        }
        mBackgroundNormal.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorNormal.getValue()));
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param colorNormalArray 颜色集合
     * @return 背景颜色
     */
    public RBaseHelper setBackgroundColorNormalArray(Color[] colorNormalArray) {
        if (colorNormalArray != null) {
            this.mBackgroundColorNormalArray = colorNormalArray.clone();
        }
        /**
         * 设置背景默认值
         */
        if (!isHasPressedBgColor) {
            mBackgroundColorPressedArray = isHasCheckedBgColor ? mBackgroundColorCheckedArray : mBackgroundColorNormalArray;
            mBackgroundPressed = setColors(mBackgroundPressed, colorArrayToIntArray(mBackgroundColorPressedArray));
        }
        if (!isHasUnableBgColor) {
            mBackgroundColorUnableArray = mBackgroundColorNormalArray;
            mBackgroundUnable = setColors(mBackgroundUnable, colorArrayToIntArray(mBackgroundColorUnableArray));
        }
        if (!isHasCheckedBgColor) {
            mBackgroundColorCheckedArray = mBackgroundColorNormalArray;
            mBackgroundChecked = setColors(mBackgroundChecked, colorArrayToIntArray(mBackgroundColorCheckedArray));
        }
        if (!isHasSelectedBgColor) {
            mBackgroundColorSelectedArray = mBackgroundColorNormalArray;
            mBackgroundSelected = setColors(mBackgroundSelected, colorArrayToIntArray(mBackgroundColorSelectedArray));
        }
        mBackgroundNormal = setColors(mBackgroundNormal, colorArrayToIntArray(mBackgroundColorNormalArray));

        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param drawableNormal 背景
     * @return 默认背景
     */
    public RBaseHelper setBackgroundDrawableNormal(Element drawableNormal) {
        this.mBackgroundNormalBmp = drawableNormal;
        /**
         * 设置背景默认值
         */
        if (!isHasPressedBgBmp) {
            mBackgroundPressedBmp = isHasCheckedBgBmp ? mBackgroundCheckedBmp : mBackgroundNormalBmp;
        }
        if (!isHasUnableBgBmp) {
            mBackgroundUnableBmp = mBackgroundNormalBmp;
        }
        if (!isHasCheckedBgBmp) {
            mBackgroundCheckedBmp = mBackgroundNormalBmp;
        }
        if (!isHasSelectedBgBmp) {
            mBackgroundSelectedBmp = mBackgroundNormalBmp;
        }
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param colorPressed 选中颜色
     * @return 颜色
     */
    public RBaseHelper setBackgroundColorPressed(Color colorPressed) {
        this.mBackgroundColorPressed = colorPressed;
        this.isHasPressedBgColor = true;
        mBackgroundPressed.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorPressed.getValue()));
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param colorPressedArray 颜色集合
     * @return 颜色
     */
    public RBaseHelper setBackgroundColorPressedArray(Color[] colorPressedArray) {
        if (colorPressedArray != null) {
            this.mBackgroundColorPressedArray = colorPressedArray.clone();
        }
        this.isHasPressedBgColor = true;
        mBackgroundPressed = setColors(mBackgroundPressed, colorArrayToIntArray(mBackgroundColorPressedArray));
        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param drawablePressed 背景图
     * @return 背景
     */
    public RBaseHelper setBackgroundDrawablePressed(Element drawablePressed) {
        this.mBackgroundPressedBmp = drawablePressed;
        this.isHasPressedBgBmp = true;
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param colorUnable 颜色未选择
     * @return 未选择颜色
     */
    public RBaseHelper setBackgroundColorUnable(Color colorUnable) {
        this.mBackgroundColorUnable = colorUnable;
        this.isHasUnableBgColor = true;
        mBackgroundUnable.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorUnable.getValue()));
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param drawableUnable 未选择图
     * @return 背景
     */
    public RBaseHelper setBackgroundDrawableUnable(Element drawableUnable) {
        this.mBackgroundUnableBmp = drawableUnable;
        this.isHasUnableBgBmp = true;
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param colorUnableArray 集合
     * @return 未选择颜色集合
     */
    public RBaseHelper setBackgroundColorUnableArray(Color[] colorUnableArray) {
        if (colorUnableArray != null) {
            this.mBackgroundColorUnableArray = colorUnableArray.clone();
        }
        this.isHasUnableBgColor = true;
        mBackgroundUnable = setColors(mBackgroundUnable, colorArrayToIntArray(mBackgroundColorUnableArray));
        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param colorChecked 颜色
     * @return 选中颜色
     */
    public RBaseHelper setBackgroundColorChecked(Color colorChecked) {
        this.mBackgroundColorChecked = colorChecked;
        this.isHasCheckedBgColor = true;
        mBackgroundChecked.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorChecked.getValue()));
        if (!isHasPressedBgColor) {
            mBackgroundColorPressed = isHasCheckedBgColor ? mBackgroundColorChecked : mBackgroundColorNormal;
            mBackgroundPressed.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorPressed.getValue()));
        }
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param colorSelected 颜色选中
     * @return 选中颜色
     */
    public RBaseHelper setBackgroundColorSelected(Color colorSelected) {
        this.mBackgroundColorSelected = colorSelected;
        this.isHasSelectedBgColor = true;
        mBackgroundSelected.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorSelected.getValue()));
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param colorCheckedArray 选中
     * @return 颜色集合
     */
    public RBaseHelper setBackgroundColorCheckedArray(Color[] colorCheckedArray) {
        if (colorCheckedArray != null) {
            this.mBackgroundColorCheckedArray = colorCheckedArray.clone();
        }
        this.isHasCheckedBgColor = true;
        mBackgroundChecked = setColors(mBackgroundChecked, colorArrayToIntArray(mBackgroundColorCheckedArray));
        if (!isHasPressedBgColor) {
            mBackgroundColorPressedArray = isHasCheckedBgColor ? mBackgroundColorCheckedArray : mBackgroundColorNormalArray;
            mBackgroundPressed = setColors(mBackgroundPressed, colorArrayToIntArray(mBackgroundColorPressedArray));
        }
        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param colorSelectedArray 颜色集合
     * @return 集合
     */
    public RBaseHelper setBackgroundColorSelectedArray(Color[] colorSelectedArray) {
        if (colorSelectedArray != null) {
            this.mBackgroundColorSelectedArray = colorSelectedArray.clone();
        }
        this.isHasSelectedBgColor = true;
        mBackgroundSelected = setColors(mBackgroundSelected, colorArrayToIntArray(mBackgroundColorSelectedArray));
        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param drawableChecked 背景
     * @return 选中背景
     */
    public RBaseHelper setBackgroundDrawableChecked(Element drawableChecked) {
        this.mBackgroundCheckedBmp = drawableChecked;
        this.isHasCheckedBgBmp = true;
        if (!isHasPressedBgBmp) {
            mBackgroundPressedBmp = mBackgroundCheckedBmp;
        }
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    /**
     * 构造
     *
     * @param drawableSelected 选中背景
     * @return 背景
     */
    public RBaseHelper setBackgroundDrawableSelected(Element drawableSelected) {
        this.mBackgroundSelectedBmp = drawableSelected;
        this.isHasSelectedBgBmp = true;
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    /**
     * 刷新StateListDrawable状态
     * 更新drawable背景时时候刷新
     */
    private void refreshStateListDrawable() {
        mStateBackground = emptyStateListDrawable;

        // unable,focused,pressed,checked,selected,normal
        mStateBackground.addState(states[0], mBackgroundUnableBmp == null ? mBackgroundUnable : mBackgroundUnableBmp);
        mStateBackground.addState(states[1], mBackgroundPressedBmp == null ? mBackgroundPressed : mBackgroundPressedBmp);
        mStateBackground.addState(states[2], mBackgroundPressedBmp == null ? mBackgroundPressed : mBackgroundPressedBmp);
        mStateBackground.addState(states[3], mBackgroundCheckedBmp == null ? mBackgroundChecked : mBackgroundCheckedBmp);
        mStateBackground.addState(states[4], mBackgroundSelectedBmp == null ? mBackgroundSelected : mBackgroundSelectedBmp);
        mStateBackground.addState(states[5], mBackgroundNormalBmp == null ? mBackgroundNormal : mBackgroundNormalBmp);
    }

    private void setBackgroundState() {
        boolean isHasCustom = false; // 是否存在自定义
        boolean isHasCusBg, hasCusBorder = false, hasCusCorner = false; // 存在自定义相关属性
        boolean isUnHasBgColor = mBackgroundColorNormal == null && mBackgroundColorUnable == null && mBackgroundColorPressed == null && mBackgroundColorChecked == null && mBackgroundColorSelected == null;
        boolean isUnHasBgColorArray = mBackgroundColorNormalArray == null && mBackgroundColorUnableArray == null && mBackgroundColorPressedArray == null && mBackgroundColorCheckedArray == null && mBackgroundColorSelectedArray == null;
        boolean isUnHasBgDrawable = mBackgroundNormalBmp == null && mBackgroundPressedBmp == null && mBackgroundUnableBmp == null && mBackgroundCheckedBmp == null && mBackgroundSelectedBmp == null;

        // 是否自定义了背景
        if (isUnHasBgColor && isUnHasBgColorArray && isUnHasBgDrawable) { // 未设置自定义背景
            isHasCusBg = false;
        } else {
            isHasCusBg = true;
        }

        // 是否自定义了边框
        if (mBorderDashWidth != 0 || mBorderDashGap != 0
                || mBorderWidthNormal != 0 || mBorderWidthPressed != 0 || mBorderWidthUnable != 0 || mBorderWidthChecked != 0 || mBorderWidthSelected != 0
                || mBorderColorNormal != Color.TRANSPARENT || mBorderColorPressed != Color.TRANSPARENT || mBorderColorUnable != Color.TRANSPARENT || mBorderColorChecked != Color.TRANSPARENT || mBorderColorSelected != Color.TRANSPARENT) {
            hasCusBorder = true;
        }

        // 是否自定义了圆角
        if (mCornerRadius != -1 || mCornerRadiusTopLeft != 0 || mCornerRadiusTopRight != 0 || mCornerRadiusBottomLeft != 0 || mCornerRadiusBottomRight != 0) {
            hasCusCorner = true;
        }

        if (isHasCusBg || hasCusCorner || hasCusBorder) {
            isHasCustom = true;
        }

        // 未设置自定义属性,获取原生背景并且设置

        if (!isHasCustom && !useRipple()) {
            mBackgroundDrawable = mComponentBackground; // 使用原生背景
        } else {
            // 获取drawable
            mBackgroundDrawable = getBackgroundDrawable(isHasCustom, mRippleColor);
        }
        mComponent.setBackground(mBackgroundDrawable);
    }

    /**
     * 获取 BackgroundDrawable
     *
     * @param isHasCustom 是否存在自定义背景
     * @param rippleColor 水波纹颜色
     * @return 获取背景的水波纹颜色
     */
    private Element getBackgroundDrawable(boolean isHasCustom, Color rippleColor) {
        if (!isUseRipple()) {
            return mStateBackground;
        } else {
            StateElement stateBackground = new StateElement();
            int[][] states = new int[6][];
            // unable,checked,selected,normal
            states[0] = new int[]{COMPONENT_STATE_DISABLED}; // unable
            states[1] = new int[]{COMPONENT_STATE_CHECKED}; // checked
            states[2] = new int[]{COMPONENT_STATE_SELECTED}; // selected
            states[3] = new int[]{COMPONENT_STATE_FOCUSED}; // FOUSED
            states[4] = new int[]{COMPONENT_STATE_PRESSED}; // PRESS
            states[5] = new int[]{COMPONENT_STATE_EMPTY}; // PRESS
            // unable,checked,normal
            stateBackground.addState(states[0], mBackgroundUnable);
            stateBackground.addState(states[1], mBackgroundChecked);
            stateBackground.addState(states[2], mBackgroundSelected);
            stateBackground.addState(states[3], mBackgroundPressed);
            stateBackground.addState(states[4], mBackgroundPressed);
            stateBackground.addState(states[5], mBackgroundNormal);
            return stateBackground;
        }
    }

    /**
     * 阴影
     *
     * @return 默认
     */
    public boolean useShadow() {
        return mShadowRadius >= 0;
    }

    /**
     * 构造
     *
     * @param shadowRadius 角度
     * @return 角度
     */
    public RBaseHelper setShadowRadius(int shadowRadius) {
        this.mShadowRadius = shadowRadius;
        setBackgroundState();
        return this;
    }

    public int getShadowRadius() {
        return mShadowRadius;
    }

    /**
     * 构造
     *
     * @param shadowColor 颜色
     * @return 颜色
     */
    public RBaseHelper setShadowColor(Color shadowColor) {
        this.mShadowColor = shadowColor;
        setBackgroundState();
        return this;
    }

    public Color getShadowColor() {
        return mShadowColor;
    }

    /**
     * 构造
     *
     * @param shadowDx 阴影x
     * @return 阴影
     */
    public RBaseHelper setShadowDx(int shadowDx) {
        this.mShadowDx = shadowDx;
        setBackgroundState();
        return this;
    }

    public int getShadowDx() {
        return mShadowDx;
    }

    /**
     * 构造
     *
     * @param shadowDy 阴影y
     * @return 阴影
     */
    public RBaseHelper setShadowDy(int shadowDy) {
        this.mShadowDy = shadowDy;
        setBackgroundState();
        return this;
    }

    public int getShadowDy() {
        return mShadowDy;
    }

    /**
     * 波纹
     *
     * @param isRipple 布尔
     * @return 上下文
     */

    public RBaseHelper setUseRipple(boolean isRipple) {
        this.isUseRipple = isRipple;
        setBackgroundState();
        return this;
    }

    /**
     * 水波纹
     *
     * @return 是否开启
     */
    public boolean useRipple() {
        return isUseRipple;
    }

    /**
     * 构造
     *
     * @param rippleColor 颜色
     * @return 水波纹颜色
     */
    public RBaseHelper setRippleColor(Color rippleColor) {
        this.mRippleColor = rippleColor;
        this.isUseRipple = true;
        setBackgroundState();
        return this;
    }

    public Color getRippleColor() {
        return mRippleColor;
    }

    /**
     * 构造
     *
     * @param rippleMaskDrawable 水波纹背景
     * @return 背景
     */
    public RBaseHelper setRippleMaskDrawable(Element rippleMaskDrawable) {
        this.mRippleMaskDrawable = rippleMaskDrawable;
        this.isUseRipple = true;
        setBackgroundState();
        return this;
    }

    public Element getRippleMaskDrawable() {
        return mRippleMaskDrawable;
    }

    /**
     * 边框
     *
     * @param width 宽度
     * @return 上下文
     */

    public RBaseHelper setBorderWidthNormal(int width) {
        this.mBorderWidthNormal = width;
        if (!isHasPressedBorderWidth) {
            mBorderWidthPressed = isHasCheckedBorderWidth ? mBorderWidthChecked : mBorderWidthNormal;
            setBorderPressed();
        }
        if (!isHasUnableBorderWidth) {
            mBorderWidthUnable = mBorderWidthNormal;
            setBorderUnable();
        }
        if (!isHasCheckedBorderWidth) {
            mBorderWidthChecked = mBorderWidthNormal;
            setBorderChecked();
        }
        if (!isHasSelectedBorderWidth) {
            mBorderWidthSelected = mBorderWidthNormal;
            setBorderSelected();
        }
        setBorderNormal();
        return this;
    }

    public int getBorderWidthNormal() {
        return mBorderWidthNormal;
    }

    /**
     * 构造
     *
     * @param color 颜色
     * @return 颜色值
     */
    public RBaseHelper setBorderColorNormal(Color color) {
        this.mBorderColorNormal = color;
        if (!isHasPressedBorderColor) {
            new ToastDialog(mContext).setText("999").show();
            mBorderColorPressed = isHasCheckedBorderColor ? mBorderColorChecked : mBorderColorNormal;
            setBorderPressed();
        }
        if (!isHasUnableBorderColor) {
            mBorderColorUnable = mBorderColorNormal;
            setBorderUnable();
        }
        if (!isHasCheckedBorderColor) {
            mBorderColorChecked = mBorderColorNormal;
            setBorderChecked();
        }
        if (!isHasSelectedBorderColor) {
            mBorderColorSelected = mBorderColorNormal;
            setBorderSelected();
        }
        setBorderNormal();
        return this;
    }

    public Color getBorderColorNormal() {
        return mBorderColorNormal;
    }

    /**
     * 构造
     *
     * @param width 宽度
     * @return 宽度
     */
    public RBaseHelper setBorderWidthPressed(int width) {
        this.mBorderWidthPressed = width;
        this.isHasPressedBorderWidth = true;
        setBorderPressed();
        return this;
    }

    public int getBorderWidthPressed() {
        return mBorderWidthPressed;
    }

    /**
     * 构造
     *
     * @param color 颜色
     * @return 颜色值
     */
    public RBaseHelper setBorderColorPressed(Color color) {
        this.mBorderColorPressed = color;
        this.isHasPressedBorderColor = true;
        setBorderPressed();
        return this;
    }

    public Color getBorderColorPressed() {
        return mBorderColorPressed;
    }

    /**
     * 构造
     *
     * @param color 颜色
     * @return 颜色值
     */
    public RBaseHelper setBorderColorChecked(Color color) {
        this.mBorderColorChecked = color;
        this.isHasCheckedBorderColor = true;
        if (!isHasPressedBorderColor) {
            mBorderColorPressed = mBorderColorChecked;
        }
        setBorderChecked();
        return this;
    }

    public Color getBorderColorChecked() {
        return mBorderColorChecked;
    }

    /**
     * 构造
     *
     * @param color 颜色
     * @return 颜色值
     */
    public RBaseHelper setBorderColorSelected(Color color) {
        this.mBorderColorSelected = color;
        this.isHasSelectedBorderColor = true;
        setBorderSelected();
        return this;
    }

    public Color getBorderColorSelected() {
        return mBorderColorSelected;
    }

    /**
     * 构造
     *
     * @param width 宽度
     * @return 宽度
     */
    public RBaseHelper setBorderWidthChecked(int width) {
        this.mBorderWidthChecked = width;
        this.isHasCheckedBorderWidth = true;
        if (!isHasPressedBorderWidth) {
            mBorderWidthPressed = mBorderWidthChecked;
        }
        setBorderChecked();
        return this;
    }

    public int getBorderWidthChecked() {
        return mBorderWidthChecked;
    }

    /**
     * 构造
     *
     * @param width 宽度
     * @return 宽度值
     */
    public RBaseHelper setBorderWidthSelected(int width) {
        this.mBorderWidthSelected = width;
        this.isHasSelectedBorderWidth = true;
        setBorderChecked();
        return this;
    }

    public int getBorderWidthSelected() {
        return mBorderWidthSelected;
    }

    /**
     * 构造
     *
     * @param width 宽度
     * @return 边框宽度
     */
    public RBaseHelper setBorderWidthUnable(int width) {
        this.mBorderWidthUnable = width;
        this.isHasUnableBorderWidth = true;
        setBorderUnable();
        return this;
    }

    public int getBorderWidthUnable() {
        return mBorderWidthUnable;
    }

    /**
     * 构造
     *
     * @param color 颜色
     * @return 值
     */
    public RBaseHelper setBorderColorUnable(Color color) {
        this.mBorderColorUnable = color;
        this.isHasUnableBorderColor = true;
        setBorderUnable();
        return this;
    }

    public Color getBorderColorUnable() {
        return mBorderColorUnable;
    }

    /**
     * 构造
     *
     * @param normal 默认
     * @param pressed 选中后
     * @param unable 未选
     * @param checked 选中
     * @param selected 选中
     * @return 宽度
     */
    public RBaseHelper setBorderWidth(int normal, int pressed, int unable, int checked,
                                      int selected) {
        this.mBorderWidthNormal = normal;
        this.mBorderWidthPressed = pressed;
        this.mBorderWidthUnable = unable;
        this.mBorderWidthChecked = checked;
        this.mBorderWidthSelected = selected;
        this.isHasPressedBorderWidth = true;
        this.isHasUnableBorderWidth = true;
        this.isHasCheckedBorderWidth = true;
        this.isHasSelectedBorderWidth = true;
        setBorder();
        return this;
    }

    /**
     * 构造
     *
     * @param normal 默认
     * @param pressed 选中后
     * @param unable 未选
     * @param checked 选中
     * @param selected 选中
     * @return 颜色
     */
    public RBaseHelper setBorderColor(Color normal, Color pressed,
                                      Color unable, Color checked, Color selected) {
        this.mBorderColorNormal = normal;
        this.mBorderColorPressed = pressed;
        this.mBorderColorUnable = unable;
        this.mBorderColorChecked = checked;
        this.mBorderColorSelected = selected;
        this.isHasPressedBorderColor = true;
        this.isHasUnableBorderColor = true;
        this.isHasCheckedBorderColor = true;
        this.isHasSelectedBorderColor = true;
        setBorder();
        return this;
    }

    /**
     * 构造
     *
     * @param dashWidth 阴影宽度
     * @return 宽度
     */
    public RBaseHelper setBorderDashWidth(float dashWidth) {
        this.mBorderDashWidth = dashWidth;
        setBorder();
        return this;
    }

    public float getBorderDashWidth() {
        return mBorderDashWidth;
    }

    /**
     * 构造
     *
     * @param dashGap 值
     * @return 阴影
     */
    public RBaseHelper setBorderDashGap(float dashGap) {
        this.mBorderDashGap = dashGap;
        setBorder();
        return this;
    }

    public float getBorderDashGap() {
        return mBorderDashGap;
    }

    /**
     * 构造
     *
     * @param dashWidth 宽度
     * @param dashGap 值
     * @return 阴影
     */
    public RBaseHelper setBorderDash(float dashWidth, float dashGap) {
        this.mBorderDashWidth = dashWidth;
        this.mBorderDashGap = dashGap;
        setBorder();
        return this;
    }

    private void setBorder() {
        float[] floats = new float[2];
        floats[0] = mBorderDashGap;
        floats[1] = mBorderDashWidth;

        mBackgroundNormal.setStroke(mBorderWidthNormal, RgbColor.fromArgbInt(mBorderColorNormal.getValue()));
        mBackgroundNormal.setDashPathEffectValues(floats, 0);
        mBackgroundPressed.setStroke(mBorderWidthPressed, RgbColor.fromArgbInt(mBorderColorPressed.getValue()));
        mBackgroundPressed.setDashPathEffectValues(floats, 0);
        mBackgroundUnable.setStroke(mBorderWidthUnable, RgbColor.fromArgbInt(mBorderColorUnable.getValue()));
        mBackgroundUnable.setDashPathEffectValues(floats, 0);
        mBackgroundChecked.setStroke(mBorderWidthChecked, RgbColor.fromArgbInt(mBorderColorChecked.getValue()));
        mBackgroundChecked.setDashPathEffectValues(floats, 0);
        mBackgroundSelected.setStroke(mBorderWidthSelected, RgbColor.fromArgbInt(mBorderColorSelected.getValue()));
        mBackgroundSelected.setDashPathEffectValues(floats, 0);
        setBackgroundState();
    }

    private void setBorderNormal() {
        mBackgroundNormal.setStroke(mBorderWidthNormal, RgbColor.fromArgbInt(mBorderColorNormal.getValue()));
        setBackgroundState();
    }

    private void setBorderPressed() {
        mBackgroundPressed.setStroke(mBorderWidthPressed, RgbColor.fromArgbInt(mBorderColorPressed.getValue()));
        setBackgroundState();
    }

    private void setBorderUnable() {
        mBackgroundUnable.setStroke(mBorderWidthUnable, RgbColor.fromArgbInt(mBorderColorUnable.getValue()));
        setBackgroundState();
    }

    private void setBorderChecked() {
        mBackgroundChecked.setStroke(mBorderWidthChecked, RgbColor.fromArgbInt(mBorderColorChecked.getValue()));
        setBackgroundState();
    }

    private void setBorderSelected() {
        mBackgroundSelected.setStroke(mBorderWidthSelected, RgbColor.fromArgbInt(mBorderColorSelected.getValue()));
        setBackgroundState();
    }

    /**
     * 圆角
     *
     * @param radius float类型
     */

    public void setCornerRadius(float radius) {
        this.mCornerRadius = radius;
        setRadiusValue();
    }

    public float getCornerRadius() {
        return mCornerRadius;
    }

    /**
     * 构造
     *
     * @param topLeft 左上
     * @return 角度
     */
    public RBaseHelper setCornerRadiusTopLeft(float topLeft) {
        this.mCornerRadius = -1;
        this.mCornerRadiusTopLeft = topLeft;
        setRadiusValue();
        return this;
    }

    public float getCornerRadiusTopLeft() {
        return mCornerRadiusTopLeft;
    }

    /**
     * 构造
     *
     * @param topRight 右上
     * @return 角度
     */
    public RBaseHelper setCornerRadiusTopRight(float topRight) {
        this.mCornerRadius = -1;
        this.mCornerRadiusTopRight = topRight;
        setRadiusValue();
        return this;
    }

    public float getCornerRadiusTopRight() {
        return mCornerRadiusTopRight;
    }

    /**
     * 构造
     *
     * @param bottomRight 右下
     * @return 角度
     */
    public RBaseHelper setCornerRadiusBottomRight(float bottomRight) {
        this.mCornerRadius = -1;
        this.mCornerRadiusBottomRight = bottomRight;
        setRadiusValue();
        return this;
    }

    public float getCornerRadiusBottomRight() {
        return mCornerRadiusBottomRight;
    }

    /**
     * 构造
     *
     * @param bottomLeft 左下
     * @return 角度
     */
    public RBaseHelper setCornerRadiusBottomLeft(float bottomLeft) {
        this.mCornerRadius = -1;
        this.mCornerRadiusBottomLeft = bottomLeft;
        setRadiusValue();
        return this;
    }

    public float getCornerRadiusBottomLeft() {
        return mCornerRadiusBottomLeft;
    }

    /**
     * 构造
     *
     * @param topLeft 左边
     * @param topRight 右上
     * @param bottomRight 右下
     * @param bottomLeft 左下
     * @return 角度
     */
    public RBaseHelper setCornerRadius(float topLeft, float topRight, float bottomRight,
                                       float bottomLeft) {
        this.mCornerRadius = -1;
        this.mCornerRadiusTopLeft = topLeft;
        this.mCornerRadiusTopRight = topRight;
        this.mCornerRadiusBottomRight = bottomRight;
        this.mCornerRadiusBottomLeft = bottomLeft;
        setRadiusValue();
        return this;
    }

    /**
     * 设置圆角UI
     */
    private void setRadiusUI() {
        mBackgroundNormal.setCornerRadiiArray(mBorderRadii);
        mBackgroundPressed.setCornerRadiiArray(mBorderRadii);
        mBackgroundUnable.setCornerRadiiArray(mBorderRadii);
        mBackgroundChecked.setCornerRadiiArray(mBorderRadii);
        mBackgroundSelected.setCornerRadiiArray(mBorderRadii);
        setBackgroundState();
    }

    /**
     * 设置圆角数值
     */
    private void setRadiusValue() {
        if (mCornerRadius >= 0) {
            mBorderRadii[0] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[1] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[2] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[3] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[4] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[5] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[6] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[7] = DensityUtils.pxToFp(mContext, mCornerRadius);
            setRadiusUI();
            return;
        }

        if (mCornerRadius < 0) {
            mBorderRadii[0] = DensityUtils.pxToFp(mContext, mCornerRadiusTopLeft);
            mBorderRadii[1] = DensityUtils.fpToPx(mContext, mCornerRadiusTopLeft);
            mBorderRadii[2] = DensityUtils.fpToPx(mContext, mCornerRadiusTopRight);
            mBorderRadii[3] = DensityUtils.fpToPx(mContext, mCornerRadiusTopRight);
            mBorderRadii[4] = DensityUtils.fpToPx(mContext, mCornerRadiusBottomRight);
            mBorderRadii[5] = DensityUtils.fpToPx(mContext, mCornerRadiusBottomRight);
            mBorderRadii[6] = DensityUtils.fpToPx(mContext, mCornerRadiusBottomLeft);
            mBorderRadii[7] = DensityUtils.fpToPx(mContext, mCornerRadiusBottomLeft);
            setRadiusUI();
            return;
        }
    }

    /**
     * 设置component大小变化监听,用来更新渐变半径
     */
    private void addOnGlobalLayoutListener() {
    }

    /**
     * 获取背景颜色
     *
     * @param attr 属性集
     * @param key 单一颜色
     * @return 背景颜色
     */
    private Object[] getBackgroundInfo(AttrSet attr, String key) {
        Color[] colors = null;

        int bgType = BG_TYPE_COLOR; // 背景类型
        String[] bgColorArray = null; // 多个颜色
        Color bgColor = null; // 单一颜色
        Element bgElement = null;
        if (strBgType.equals("color")) {
            Log.i("单个颜色");
            bgType = BG_TYPE_COLOR;
            bgColor = getStringColor(attr, key, "#ffffff");
        } else if (strBgType.equals("colorArray")) {
            String colorList = AttrUtils.getXmlManyValue(attr, key, null);
            Log.i("多个颜色值：" + colorList);
            if (colorList != null) {
                bgColorArray = colorList.split(",");
                colors = new Color[bgColorArray.length];
                for (int i = 0; i < bgColorArray.length; i++) {
                    colors[i] = new Color(Color.getIntColor(bgColorArray[i]));
                }
                bgType = BG_TYPE_COLOR_ARRAY;
            }
        } else if (strBgType.equals("element")) {
            Log.i("背景是图片");
            bgType = BG_TYPE_IMG;

            // 图片背景类型
            bgElement = AttrUtils.getElement(attr, key, null);
            Log.i("anananan" + bgElement);
        }

        return new Object[]{bgType, bgColor, colors, bgElement};
    }

    /**
     * 获取渐变方向
     *
     * @param attr 属性集
     * @return 返回对应的渐变方向
     */
    private ShapeElement.Orientation getGradientOrientation(AttrSet attr) {
        String orientationValue = AttrUtils.getString(attr, mGradient, BL_TR).toUpperCase();
        ShapeElement.Orientation orientation = ShapeElement.Orientation.BOTTOM_LEFT_TO_TOP_RIGHT;
        switch (orientationValue) {
            case TOP_BOTTOM:
                orientation = ShapeElement.Orientation.TOP_TO_BOTTOM;
                break;
            case TR_BL:
                orientation = ShapeElement.Orientation.TOP_RIGHT_TO_BOTTOM_LEFT;
                break;
            case RIGHT_LEFT:
                orientation = ShapeElement.Orientation.RIGHT_TO_LEFT;
                break;
            case BR_TL:
                orientation = ShapeElement.Orientation.BOTTOM_RIGHT_TO_TOP_LEFT;
                break;
            case BOTTOM_TOP:
                orientation = ShapeElement.Orientation.BOTTOM_TO_TOP;
                break;
            case BL_TR:
                orientation = ShapeElement.Orientation.BOTTOM_LEFT_TO_TOP_RIGHT;
                break;
            case LEFT_RIGHT:
                orientation = ShapeElement.Orientation.LEFT_TO_RIGHT;
                break;
            case TL_BR:
                orientation = ShapeElement.Orientation.TOP_LEFT_TO_BOTTOM_RIGHT;
                break;
        }
        return orientation;
    }

    /**
     * 设置GradientDrawable颜色数组,兼容版本
     *
     * @param element GradientDrawable
     * @param colors 颜色数组
     * @return 背景颜色
     */
    private ShapeElement setColors(ShapeElement element, int[] colors) {
        if (element == null) {
            element = new ShapeElement();
        }
        element.setOrientation(mGradientOrientation);
        RgbColor[] rgbColors = new RgbColor[colors.length];
        for (int i = 0; i < colors.length; i++) {
            RgbColor color = RgbColor.fromArgbInt(colors[i]);
            rgbColors[i] = color;
        }
        element.setRgbColors(rgbColors);
        return element;
    }

    /**
     * 是否移出component
     *
     * @param x x
     * @param y y
     * @return 数值
     */
    protected boolean isOutside(int x, int y) {
        boolean isFlag = false;
        if ((x < 0 - mTouchSlop) || (x >= mComponent.getWidth() + mTouchSlop) ||
                (y < 0 - mTouchSlop) || (y >= mComponent.getHeight() + mTouchSlop)) {
            isFlag = true;
        }
        return isFlag;
    }

    /**
     * 是否使用Ripple
     *
     * @return true 是，反之false
     */
    private boolean isUseRipple() {
        return isUseRipple;
    }

    /**
     * 初始化Clip
     */
    private void initClip() {
        mClipHelper.initClip(mComponent, isClipLayout, new ClipPathManager.ClipPathCreator() {
            @Override
            public Path createClipPath(int width, int height) {
                Path path = new Path();
                path.addRoundRect(new RectFloat(0, 0, width, height), mBorderRadii, Path.Direction.COUNTER_CLOCK_WISE);
                return path;
            }
        });
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        mClipHelper.dispatchDraw(canvas);
    }

    @Override
    public void onLayout(int left, int top, int right, int bottom) {
        mClipHelper.onLayout(left, top, right, bottom);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if (isUseRipple && mComponent.isEnabled()) {
            pointerPosition = event.getPointerPosition(0);
            float x = pointerPosition.getX();
            float y = pointerPosition.getY();
            int[] parentLocationOnScreen = mComponent.getLocationOnScreen();
            int yy = (int) (y - parentLocationOnScreen[1]);
            sx = x - parentLocationOnScreen[0];
            sy = yy + 300;
            animatorValue.start();
        }
        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (isUseRipple && mComponent.isEnabled()) {
            animatorValue.setValueUpdateListener((animatorValue, v) -> {
                zj = v;
                if (v > 0.85) {
                    paint.setAlpha(0f);
                } else {
                    paint.setAlpha(0.2f);
                }
                mComponent.invalidate();
            });
            if (mComponent.getWidth() > mComponent.getHeight()) {
                canvas.drawCircle(sx, sy, mComponent.getWidth() * zj, paint);
            } else {
                canvas.drawCircle(sx, sy, mComponent.getHeight() * zj, paint);
            }
        }
    }

    @Override
    public void onRefreshed(Component component) {
    }
}
