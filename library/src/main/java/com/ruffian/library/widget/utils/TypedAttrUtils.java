/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ruffian.library.widget.utils;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.NoSuchElementException;

public class TypedAttrUtils {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x12345, "yuki");

    private TypedAttrUtils() {
    }

    /**
     * 颜色
     *
     * @param attrs 属性集
     * @param attrName 值
     * @param defValue 值
     * @return 颜色值
     */
    public static int getIntColor(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue().getValue();
        }
    }

    /**
     * 获取颜色
     *
     * @param attrs 值
     * @param attrName 名字
     * @param defValue 颜色
     * @return 颜色
     */
    public static Color getColor(AttrSet attrs, String attrName, Color defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue();
        }
    }

    /**
     * 是否开启
     *
     * @param attrs 属性集
     * @param attrName 值
     * @param isDefValue 值
     * @return true是，反之
     */
    public static boolean getBoolean(AttrSet attrs, String attrName, boolean isDefValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return isDefValue;
        } else {
            return attr.getBoolValue();
        }
    }

    /**
     * string 类型
     *
     * @param attrs 属性集
     * @param attrName 值
     * @param defValue 值
     * @return 信息
     */
    public static String getString(AttrSet attrs, String attrName, String defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getStringValue();
        }
    }

    /**
     * 颜色
     *
     * @param attrs 属性集
     * @param attrName 值
     * @param defValue 值
     * @return 颜色值
     */
    public static String getStringColor(AttrSet attrs, String attrName, String defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getStringValue();
        }
    }

    /**
     * 图片
     *
     * @param attrs 属性集
     * @param attrName 值
     * @param defValue 值
     * @return 图片方向
     */
    public static int getDimensionPixelSize(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    /**
     * 字体大小
     *
     * @param attrs 属性集
     * @param attrName 值
     * @param defValue 值
     * @return 返回大小
     */
    public static float getFloatSize(AttrSet attrs, String attrName, float defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getFloatValue();
        }
    }

    /**
     * 图片类型
     *
     * @param attrs 属性集
     * @param attrName 值
     * @param defValue 值
     * @return 图片数据
     */
    public static Element getDrawable(AttrSet attrs, String attrName, Element defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getElement();
        }
    }

    /**
     * 获取布局
     *
     * @param attrs 属性集
     * @param attrName 值
     * @param defValue 值
     * @return 返回结果
     */
    public static int getLayoutDimension(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getDimensionValue();
        }
    }

    private static Attr attrNoSuchElement(AttrSet attrs, String attrName) {
        Attr attr = null;
        try {
            attr = attrs.getAttr(attrName).get();
            Log.i("===:" + attr.getName());
        } catch (NoSuchElementException e) {
            HiLog.info(LABEL, "Exception = " + e.toString());
        }
        return attr;
    }
}
