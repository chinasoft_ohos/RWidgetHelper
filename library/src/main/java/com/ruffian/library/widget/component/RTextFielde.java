package com.ruffian.library.widget.component;

import com.ruffian.library.widget.helper.RTextHelper;
import com.ruffian.library.widget.iface.RHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * REditText
 *
 * @author ZhongDaFeng
 */
public class RTextFielde extends TextField implements RHelper<RTextHelper>,Component.TouchEventListener {
    private RTextHelper mHelper;

    /**
     * 构造
     *
     * @param context 上下文
     */
    public RTextFielde(Context context) {
        this(context, null);
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrs 属性集
     */
    public RTextFielde(Context context, AttrSet attrs) {
        super(context, attrs);
        mHelper = new RTextHelper(context, this, attrs);
    }

    @Override
    public RTextHelper getHelper() {
        return mHelper;
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        super.setEnabled(isEnabled);
        if (mHelper != null) {
            mHelper.setEnabled(isEnabled);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if (mHelper != null) {
            mHelper.onTouchEvent(component, event);
        }
        return super.getTouchEventListener().onTouchEvent(component, event);
    }

    @Override
    public void setSelected(boolean isSelected) {
        if (mHelper != null) {
            mHelper.setSelected(isSelected);
        }
        super.setSelected(isSelected);
    }
}
