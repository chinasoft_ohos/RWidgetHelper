package com.ruffian.library.widget.component;

import com.ruffian.library.widget.utils.AttrUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

public class RImage extends Image implements Component.DrawTask {
    // 边框
    private float mBorderWidth = 0;
    private Color mBorderColor = Color.BLACK;

    // 是否圆形
    private boolean mIsCircle = false;
    private Element mDrawable;
    private ScaleMode mScaleType;
    private PixelMap bitmap;

    /**
     * 构造
     *
     * @param context 上下文
     */
    public RImage(Context context) {
        super(context);
        addDrawTask(this::onDraw);
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrs 属性集
     */
    public RImage(Context context, AttrSet attrs) {
        super(context, attrs);
        addDrawTask(this::onDraw);
        initAttributeSet(attrs);
    }

    /**
     * 初始化自定义属性
     *
     * @param attrs 属性集
     */
    private void initAttributeSet(AttrSet attrs) {
        mIsCircle = AttrUtils.getBoolean(attrs, "is_circle", false);
        mBorderWidth = AttrUtils.getInt(attrs, "border_width", 0);
        mBorderColor = AttrUtils.getColor(attrs, "border_color", Color.WHITE);
        updateDrawableAttrs();
    }

    @Override
    public void setScaleMode(ScaleMode scaleType) {
        if (mScaleType != scaleType) {
            mScaleType = scaleType;
            updateDrawableAttrs();
            invalidate();
        }
    }

    private void updateDrawableAttrs() {
        setPixelMap(toRoundBitmap());
    }

    /**
     * 绘制空Bitmap
     */
    private void drawEmptyBitmap() {
        if (mDrawable == null) { // 未设置Bitmap
            int width = getEstimatedWidth();
            int height = getEstimatedHeight();
            if (width > 0 && height > 0) {
                Element background = getBackgroundElement();
                if (background != null) {
                    background.setBounds(0, 0, width, height);
                    setImageElement(background);
                } else {
                    PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
                    initializationOptions.size = new Size(width, height);
                    initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
                    setPixelMap(PixelMap.create(initializationOptions));
                }
            }
        }
    }

    /**
     * 构造
     *
     * @param isCircle 是否圆
     * @return true类型
     */
    public RImage isCircle(boolean isCircle) {
        this.mIsCircle = isCircle;
        updateDrawableAttrs();
        return this;
    }

    /**
     * 获取边框的宽度
     *
     * @return 边框的宽度
     */
    public float getBorderWidth() {
        return mBorderWidth;
    }

    /**
     * 构造
     *
     * @param borderWidth 宽度
     * @return 宽度值
     */
    public RImage setBorderWidth(int borderWidth) {
        this.mBorderWidth = borderWidth;
        updateDrawableAttrs();
        return this;
    }

    public Color getBorderColor() {
        return mBorderColor;
    }

    /**
     * 构造
     *
     * @param borderColor 颜色
     * @return 颜色值
     */
    public RImage setBorderColor(Color borderColor) {
        this.mBorderColor = borderColor;
        updateDrawableAttrs();
        return this;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mIsCircle) {
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE_STYLE);
            paint.setStrokeWidth(20);
        }
    }

    @Override
    public void setPixelMap(PixelMap pixelMap) {
        super.setPixelMap(pixelMap);
        this.bitmap = pixelMap;
    }

    private PixelMap toRoundBitmap() {
        // 获取宽高
        int width = bitmap.getImageInfo().size.width;
        int height = bitmap.getImageInfo().size.height;
        // 画一个圆
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.size = new Size(width, height);
        PixelMap output = PixelMap.create(options);
        Texture texture = new Texture(output);
        Canvas canvas = new Canvas(texture);
        if (mIsCircle) {
            int rc = Math.min(width, height);
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(Color.BLUE);
            // 图片
            canvas.drawCircle((float) ((width) / 2d), (float) ((height) / 2d), (float) ((rc) / 2d - mBorderWidth), paint);

            paint.reset();
            paint.setAntiAlias(true);
            paint.setBlendMode(BlendMode.SRC_IN);
            PixelMapHolder holder = new PixelMapHolder(this.bitmap);
            canvas.drawPixelMapHolder(holder, 0, 0, paint);

            paint.reset();
            paint.setAntiAlias(true);
            paint.setStrokeWidth(mBorderWidth);
            paint.setColor(mBorderColor);
            paint.setStyle(Paint.Style.STROKE_STYLE);
            // 边框
            canvas.drawCircle((float) ((width) / 2d), (float) ((height) / 2d), (float) ((rc) / 2d - mBorderWidth), paint);
        } else {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.STROKE_STYLE);
            PixelMapHolder holder = new PixelMapHolder(bitmap);
            canvas.drawPixelMapHolder(holder, 0, 0, paint);
            paint.reset();
            paint.setStyle(Paint.Style.STROKE_STYLE);
            paint.setAntiAlias(true);
            paint.setStrokeWidth(mBorderWidth);
            paint.setColor(mBorderColor);
            RectFloat rectFloat = new RectFloat();
            rectFloat.left = 0;
            rectFloat.bottom = bitmap.getImageInfo().size.height;
            rectFloat.right = bitmap.getImageInfo().size.width;
            rectFloat.top = 0;
            canvas.drawRoundRect(rectFloat, 30, 30, paint);
        }
        return output;
    }
}
