package com.ruffian.library.widget.component;

import com.ruffian.library.widget.helper.RCheckHelper;
import com.ruffian.library.widget.iface.RHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.RadioButton;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * RRadioButton
 *
 * @author ZhongDaFeng
 */
public class RRadioButton extends RadioButton implements RHelper<RCheckHelper>, Component.TouchEventListener {
    private RCheckHelper mHelper;

    /**
     * 构造
     *
     * @param context 上下文
     */
    public RRadioButton(Context context) {
        this(context, null);
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrs 属性集
     */
    public RRadioButton(Context context, AttrSet attrs) {
        super(context, attrs);
        mHelper = new RCheckHelper(context, this, attrs);
    }

    @Override
    public RCheckHelper getHelper() {
        return mHelper;
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        super.setEnabled(isEnabled);
        if (mHelper != null) {
            mHelper.setEnabled(isEnabled);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if (mHelper != null) {
            mHelper.onTouchEvent(component, event);
        }
        return super.getTouchEventListener().onTouchEvent(component, event);
    }

    @Override
    public void setSelected(boolean isSelected) {
        if (mHelper != null) {
            mHelper.setSelected(isSelected);
        }
        super.setSelected(isSelected);
    }

    @Override
    public void setChecked(boolean isChecked) {
        if (mHelper != null) {
            mHelper.setChecked(isChecked);
        }
        super.setChecked(isChecked);
    }
}
