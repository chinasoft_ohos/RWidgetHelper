package com.ruffian.library.widget.component;

import com.ruffian.library.widget.helper.RBaseHelper;
import com.ruffian.library.widget.iface.RHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.render.Canvas;
import ohos.app.Context;

/**
 * RLinearLayout
 *
 * @author ZhongDaFeng
 */
public class RDirectionLayout extends DirectionalLayout implements RHelper<RBaseHelper>, Component.DrawTask {
    private RBaseHelper<RDirectionLayout> mHelper;

    /**
     * 构造
     *
     * @param context 上下文
     */
    public RDirectionLayout(Context context) {
        this(context, null);
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrs 属性集
     */
    public RDirectionLayout(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrs 属性集
     * @param defStyleAttr 默认值
     */
    public RDirectionLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addDrawTask(this::onDraw);
        mHelper = new RBaseHelper<>(context, this, attrs);
    }

    @Override
    public RBaseHelper getHelper() {
        return mHelper;
    }

    @Override
    public void arrange(int left, int top, int width, int height) {
        super.arrange(left, top, width, height);
        mHelper.onLayout(left, top, width, height);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mHelper.dispatchDraw(canvas);
    }
}
