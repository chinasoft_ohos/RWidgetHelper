package com.ruffian.library.widget.clip;

import ohos.agp.render.Path;

public class ClipPathManager {
    protected final Path path = new Path();

    /**
     * 构造方法
     */
    public ClipPathManager() {
    }

    /**
     * 设置裁剪布局
     *
     * @param width 宽
     * @param height 高
     */
    public void setupClipLayout(int width, int height) {
        path.reset();
        Path clipPath = null;
        if (createClipPath != null) {
            clipPath = createClipPath.createClipPath(width, height);
        }
        if (clipPath != null) {
            path.set(clipPath);
        }
    }

    /**
     * 设置裁剪路径
     *
     * @return 路径
     */
    public Path getClipPath() {
        return path;
    }

    /**
     * 创建Path接口
     */
    private ClipPathCreator createClipPath = null;

    public void setClipPathCreator(ClipPathCreator createClipPath) {
        this.createClipPath = createClipPath;
    }

    public interface ClipPathCreator {
        Path createClipPath(int width, int height);
    }
}
