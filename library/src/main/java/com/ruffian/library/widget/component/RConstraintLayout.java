package com.ruffian.library.widget.component;

import com.ruffian.library.widget.helper.RBaseHelper;
import com.ruffian.library.widget.iface.RHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.agp.render.Canvas;
import ohos.app.Context;

/**
 * RConstraintLayout
 *
 * @author ZhongDaFeng
 */
public class RConstraintLayout extends StackLayout implements RHelper<RBaseHelper> {
    private RBaseHelper mHelper;

    /**
     * 构造
     *
     * @param context 上下文
     */
    public RConstraintLayout(Context context) {
        this(context, null);
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrSet 属性集
     */
    public RConstraintLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrSet 属性集
     * @param styleName string值
     */
    public RConstraintLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mHelper = new RBaseHelper<>(context, this, attrSet);
    }

    @Override
    public RBaseHelper getHelper() {
        return mHelper;
    }

    @Override
    public void arrange(int left, int top, int width, int height) {
        super.arrange(left, top, width, height);
        mHelper.onLayout(left, top, width, height);
    }
}
