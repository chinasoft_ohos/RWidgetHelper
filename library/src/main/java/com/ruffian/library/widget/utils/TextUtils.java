package com.ruffian.library.widget.utils;

import ohos.agp.components.Text;
import ohos.agp.render.Paint;

import java.util.ArrayList;
import java.util.Collections;

/**
 * TextView工具类
 *
 * @author ZhongDaFeng
 */
public class TextUtils {
    private static TextUtils instance = null;
    private Paint paint;

    private TextUtils() {
    }

    /**
     * 获取工具类
     *
     * @return 单列
     */
    public static TextUtils get() {
        if (instance == null) {
            instance = new TextUtils();
        }
        return instance;
    }

    /**
     * 获取Text实际宽度
     *
     * @param text text
     * @param drawableWidth drawable宽度
     * @param paddingLeft 内边距-左
     * @param paddingRight 内边距-右
     * @param drawablePaddingHorizontal 水平方向上drawable间距
     * @return 宽度
     */
    public float getTextWidth(Text text, int drawableWidth, int paddingLeft,
                              int paddingRight, int drawablePaddingHorizontal) {
        if (text == null) {
            return 0;
        }
        paint = new Paint();
        float textWidth;

        String originalText = text.getText();
        if (originalText.contains("\n")) {
            String[] originalTextArray;
            ArrayList<Float> widthList;
            originalTextArray = originalText.split("\n");
            int arrayLen = originalTextArray.length;
            widthList = new ArrayList<>(arrayLen);
            for (int i = 0; i < arrayLen; i++) {
                widthList.add(paint.measureText(originalTextArray[i]));
            }
            textWidth = Collections.max(widthList);
        } else {
            textWidth = paint.measureText(originalText);
            LogUtil.i("sizewidth" + textWidth);
        }

        // 计算自动换行临界值，不允许超过换行临界值
        int maxWidth = text.getWidth() - drawableWidth - paddingLeft - paddingRight - drawablePaddingHorizontal;
        if (textWidth > maxWidth) {
            textWidth = maxWidth;
        }
        return textWidth;
    }

    /**
     * 获取Text实际高度
     *
     * @param text text
     * @param drawableHeight drawable高度
     * @param paddingTop 内边距-左
     * @param paddingBottom 内边距-右
     * @param drawablePaddingVertical 垂直方向上drawabl e间距
     * @return 宽高
     */
    public float getTextHeight(Text text, int drawableHeight, int paddingTop, int paddingBottom, int drawablePaddingVertical) {
        if (text == null) {
            return 0;
        }

        // 1.单行高度行数2.最大高度临界值
        Paint paint = new Paint();

        // 单行高度
        float singleLineHeight = Math.abs(
                (paint.getFontMetrics().bottom - paint.getFontMetrics().top));
        float textHeight = singleLineHeight * text.getMaxTextLines();

        // 最大高度临界值，不允许超过最大高度临界值
        int maxHeight = text.getHeight() - drawableHeight - paddingTop
                - paddingBottom - drawablePaddingVertical; // 最大允许的宽度
        if (textHeight > maxHeight) {
            textHeight = maxHeight;
        }
        return textHeight;
    }
}
