package com.ruffian.library.widget.helper;

import com.ruffian.library.widget.utils.AttrUtils;
import com.ruffian.library.widget.utils.TypedAttrUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Check-Helper
 *
 * @author ZhongDaFeng
 */
public class RCheckHelper extends RTextHelper implements Component.TouchEventListener, Component.DrawTask {
    // Text
    private Color mTextColorChecked;
    private boolean isHasCheckedTextColor = false;
    private Element mIconCheckedLeft; // Icon
    private Element mIconCheckedRight;
    private Element mIconCheckedTop;
    private Element mIconCheckedBottom;
    private Element mIconChecked; // 版本兼容
    private Element checkedIdLeft = null;
    private Element checkedIdRight = null;
    private Element checkedIdTop = null;
    private Element checkedIdBottom = null;
    private Element checkedId = null;
    private boolean isRippleBtn;
    private float zj;
    private int sy;
    private float sx;
    AnimatorValue animatorValue = new AnimatorValue();
    private Paint paint;
    private Color text_ripple_color;
    private boolean isTouch;

    {
        // 先于构造函数重置
        states = new int[6][];
    }

    /**
     * 提交
     *
     * @param context 上下文
     * @param component component
     * @param attrs 属性集
     */
    public RCheckHelper(Context context, Component component, AttrSet attrs) {
        super(context, (Text) component, attrs);
        mComponent.addDrawTask(this::onDraw);
        paint = new Paint();
        paint.setAlpha(0.2f);
        paint.setStyle(Paint.Style.FILL_STYLE);
        animatorValue.setDuration(500);
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
        initAttributeSet(context, attrs);
    }

    /**
     * 初始化控件属性
     *
     * @param context 上下文
     * @param attrs 属性集
     */

    private void initAttributeSet(Context context, AttrSet attrs) {
        if (context == null || attrs == null) {
            setup();
            return;
        }
        isRippleBtn = AttrUtils.getStringToBool(attrs, "radio_ripple", true);
        mTextColorChecked = AttrUtils.getColor(attrs, "text_color_checked", null); // text
        mIconCheckedLeft = TypedAttrUtils.getDrawable(attrs, "icon_checked_left", null); // icon
        mIconCheckedRight = TypedAttrUtils.getDrawable(attrs, "icon_checked_right", null);
        mIconCheckedTop = TypedAttrUtils.getDrawable(attrs, "icon_checked_top", null);
        mIconCheckedBottom = TypedAttrUtils.getDrawable(attrs, "icon_checked_bottom", null);
        mIconChecked = TypedAttrUtils.getDrawable(attrs, "icon_src_checked", null);
        text_ripple_color = AttrUtils.getStringToColor(attrs, "text_ripple_color", Color.TRANSPARENT);
        checkedIdLeft = TypedAttrUtils.getDrawable(attrs, "icon_checked_left", checkedIdLeft);
        checkedIdRight = TypedAttrUtils.getDrawable(attrs, "icon_checked_right", checkedIdRight);
        checkedIdTop = TypedAttrUtils.getDrawable(attrs, "icon_checked_top", checkedIdTop);
        checkedIdBottom = TypedAttrUtils.getDrawable(attrs, "icon_checked_bottom", checkedIdBottom);
        paint.setColor(text_ripple_color);
        if (checkedIdLeft != null) {
            mIconCheckedLeft = checkedIdLeft;
        }

        if (checkedIdRight != null) {
            mIconCheckedRight = checkedIdRight;
        }
        if (checkedIdTop != null) {
            mIconCheckedTop = checkedIdTop;
        }
        if (checkedIdBottom != null) {
            mIconCheckedBottom = checkedIdBottom;
        }
        checkedId = TypedAttrUtils.getDrawable(attrs, "icon_src_checked", checkedId);
        if (checkedId != null) {
            mIconChecked = checkedId;
        }
        isHasCheckedTextColor = mTextColorChecked != null;
        setup();
    }

    private void setup() {
        if (isChecked()) {
            setIconLeft(mIconCheckedLeft);
            setIconRight(mIconCheckedRight);
            setIconTop(mIconCheckedTop);
            setIconBottom(mIconCheckedBottom);
            setIcon(mIconChecked);
        }

        if (!isHasCheckedTextColor) {
            mTextColorChecked = mTextColorNormal;
        }
        initPressedTextColor(isHasCheckedTextColor, mTextColorChecked);

        states[0] = new int[]{-ComponentState.COMPONENT_STATE_DISABLED}; // unable

        states[1] = new int[]{ComponentState.COMPONENT_STATE_FOCUSED}; // focused

        states[2] = new int[]{ComponentState.COMPONENT_STATE_PRESSED}; // pressed

        states[3] = new int[]{ComponentState.COMPONENT_STATE_CHECKED}; // checked

        states[4] = new int[]{ComponentState.COMPONENT_STATE_SELECTED}; // selected

        states[5] = new int[]{ComponentState.COMPONENT_STATE_EMPTY}; // normal

        // 设置文本颜色
        setTextColor();
    }

    @Override
    public RCheckHelper setTextColorNormal(Color textColor) {
        if (!isHasCheckedTextColor) {
            mTextColorChecked = textColor;
        }
        super.setTextColorNormal(textColor);
        initPressedTextColor(isHasCheckedTextColor, mTextColorChecked);
        setTextColor();
        return this;
    }

    /**
     * 构造
     *
     * @param textColor 文字颜色
     * @return 颜色
     */
    public RCheckHelper setTextColorChecked(Color textColor) {
        this.mTextColorChecked = textColor;
        this.isHasCheckedTextColor = true;
        initPressedTextColor(isHasCheckedTextColor, mTextColorChecked);
        setTextColor();
        return this;
    }

    public Color getTextColorChecked() {
        return mTextColorChecked;
    }

    /**
     * 构造
     *
     * @param normal 默认
     * @param pressed 选中后
     * @param unable 未选中
     * @param checked 选中
     * @param selected 选中
     * @return 颜色
     */
    public RCheckHelper setTextColor(Color normal, Color pressed, Color unable, Color checked, Color selected) {
        this.mTextColorChecked = checked;
        this.isHasCheckedTextColor = true;
        super.setTextColor(normal, pressed, unable, selected);
        return this;
    }

    @Override
    protected void setTextColor() {
        // unable,focused,pressed,checked,selected,normal
        mComponent.setTextColor(mTextColorNormal);
    }

    /**
     * 构造
     *
     * @param icon 背景
     * @return 全部背景
     */
    @Deprecated
    public RCheckHelper setIconChecked(Element icon) {
        this.mIconChecked = icon;
        setIcon(icon);
        return this;
    }

    @Deprecated
    public Element getIconChecked() {
        return mIconChecked;
    }

    /**
     * 构造
     *
     * @param icon 背景
     * @return 背景
     */
    public RCheckHelper setIconCheckedLeft(Element icon) {
        this.mIconCheckedLeft = icon;
        setIconLeft(icon);
        return this;
    }

    /**
     * 构造
     *
     * @param icon 背景
     * @return 背景右边
     */
    public RCheckHelper setIconCheckedRight(Element icon) {
        this.mIconCheckedRight = icon;
        setIconRight(icon);
        return this;
    }

    /**
     * 构造
     *
     * @param icon 背景
     * @return 背景上
     */
    public RCheckHelper setIconCheckedTop(Element icon) {
        this.mIconCheckedTop = icon;
        setIconTop(icon);
        return this;
    }

    /**
     * 构造
     *
     * @param icon 背景
     * @return 下背景
     */
    public RCheckHelper setIconCheckedBottom(Element icon) {
        this.mIconCheckedBottom = icon;
        setIconBottom(icon);
        return this;
    }

    public Element getIconCheckedLeft() {
        return mIconCheckedLeft;
    }

    public Element getIconCheckedRight() {
        return mIconCheckedRight;
    }

    public Element getIconCheckedTop() {
        return mIconCheckedTop;
    }

    public Element getIconCheckedBottom() {
        return mIconCheckedBottom;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (!isChecked()) {
            if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                if (!isRippleBtn) {
                    sx = (float) (component.getWidth()/2d);
                    sy = component.getHeight()/ 2;
                    animatorValue.start();
                    isTouch = false;
                    mComponent.setTextColor(mTextColorChecked);
                }
                return true;
            }
        }
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP) {
            if (!isRippleBtn) {
                isTouch = true;
                paint.setAlpha(0f);
                mComponent.setTextColor(mTextColorNormal);
                mComponent.invalidate();
            }
        }
        return false;
    }

    private boolean isChecked() {
        if (mComponent != null) {
            return ((AbsButton) mComponent).isChecked();
        }
        return false;
    }

    /**
     * 选中
     *
     * @param isChecked true是，反之
     */
    @SuppressWarnings("unchecked")
    public void setChecked(boolean isChecked) {
        setIconLeft(isChecked ? mIconCheckedLeft : getIconNormalLeft());
        setIconRight(isChecked ? mIconCheckedRight : getIconNormalRight());
        setIconTop(isChecked ? mIconCheckedTop : getIconNormalTop());
        setIconBottom(isChecked ? mIconCheckedBottom : getIconNormalBottom());
        setIcon(isChecked ? mIconChecked : getIconNormal());
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        super.onDraw(component, canvas);
        if (!isRippleBtn) {
            animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    zj = v;
                    if (v > 0.85 && isTouch) {
                        paint.setAlpha(0f);
                    } else {
                        paint.setAlpha(0.2f);
                    }

                    mComponent.invalidate();
                }
            });
            if (mComponent.getWidth() > mComponent.getHeight()) {
                canvas.drawCircle(sx, sy, mComponent.getWidth() * zj, paint);
            } else {
                canvas.drawCircle(sx, sy, mComponent.getHeight() * zj, paint);
            }
        }
    }

    @Override
    protected boolean isSingleDirection() {
        return super.isSingleDirection() || (mIconChecked != null);
    }
}
