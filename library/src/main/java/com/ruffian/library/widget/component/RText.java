package com.ruffian.library.widget.component;

import com.ruffian.library.widget.helper.RTextHelper;
import com.ruffian.library.widget.iface.RHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * RTextView
 *
 * @author ZhongDaFeng
 */
public class RText extends Text implements RHelper<RTextHelper>{
    private RTextHelper mHelper;

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrSet 属性集
     * @param styleName string值
     */
    public RText(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * 构造
     *
     * @param context 上下文
     */
    public RText(Context context) {
        this(context, null);
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrs 属性集
     */
    public RText(Context context, AttrSet attrs) {
        super(context, attrs);
        mHelper = new RTextHelper(context, this, attrs);
    }

    @Override
    public RTextHelper getHelper() {
        return mHelper;
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        super.setEnabled(isEnabled);
        if (mHelper != null) {
            mHelper.setEnabled(isEnabled);
        }
    }

    @Override
    public void setSelected(boolean selected) {
        if (mHelper != null) {
            mHelper.setSelected(selected);
        }
        super.setSelected(selected);
    }

    @Override
    public boolean isTextCursorVisible() {
        return super.isTextCursorVisible();
    }

    @Override
    public void setVisibility(int visibility) {
        if (mHelper != null) {
            mHelper.onVisibilityChanged(null, visibility);
        }
        super.setVisibility(visibility);
    }
}
