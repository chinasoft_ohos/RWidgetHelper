package com.ruffian.library.widget.clip;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.BlendMode;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;

/**
 * ClipHelper
 *
 * @author ZhongDaFeng
 */
public class ClipHelper implements IClip {
    private final Paint clipPaint = new Paint();
    private final Path clipPath = new Path();
    private final Path rectView = new Path();
    private ClipPathManager clipPathManager = new ClipPathManager();

    private boolean isRequestShapeUpdate = true;
    private Component mComponent;
    private boolean isClipLayout;

    /**
     * 构造
     */
    public ClipHelper() {
        clipPaint.setAntiAlias(true);
        clipPaint.setColor(Color.BLUE);
        clipPaint.setStyle(Paint.Style.FILL_STYLE);
        clipPaint.setStrokeWidth(1);
    }

    /**
     * 初始化Clip
     *
     * @param component component
     * @param isClipLayout 布尔
     * @param clipPathCreator 值
     */
    public void initClip(Component component, boolean isClipLayout, ClipPathManager.ClipPathCreator clipPathCreator) {
        this.mComponent = component;
        this.isClipLayout = isClipLayout;
        if (!canClip()) {
            return;
        }
        clipPaint.setBlendMode(BlendMode.DST_IN);

        // 设置clip
        clipPathManager.setClipPathCreator(clipPathCreator);
        requestShapeUpdate();
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        if (!canClip()) {
            return;
        }
        if (isRequestShapeUpdate) {
            calculateLayout(canvas.getLocalClipBounds().getWidth(), canvas.getLocalClipBounds().getHeight());
            isRequestShapeUpdate = false;
        }
        canvas.drawPath(clipPath, clipPaint);
    }

    @Override
    public void onLayout(int left, int top, int right, int bottom) {
        if (!canClip()) {
            return;
        }
    }

    private void calculateLayout(int width, int height) {
        rectView.reset();
        rectView.addRect(0f, 0f, 1f * getComponent().getWidth(), 1f * getComponent().getHeight(), Path.Direction.CLOCK_WISE);

        if (width > 0 && height > 0) {
            clipPathManager.setupClipLayout(width, height);
            clipPath.reset();
            clipPath.set(clipPathManager.getClipPath());
        }
        getComponent().invalidate();
    }

    /**
     * 请求更新
     */
    public void requestShapeUpdate() {
        this.isRequestShapeUpdate = true;
        getComponent().invalidate();
    }

    public Component getComponent() {
        return mComponent;
    }

    /**
     * 是否满足裁剪条件
     *
     * @return true 满足反之
     */
    public boolean canClip() {
        return getComponent() != null && getComponent() instanceof ComponentContainer && isClipLayout;
    }
}
