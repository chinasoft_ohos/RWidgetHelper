# RWidgetHelper

## 项目介绍
- 项目名称：RWidgetHelper
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现多种UI:圆角、边框、渐变、图形的角度、背景色，字体颜色、渐变、水波纹、阴影、自定义类型的单选和多选
- 项目移植状态：主功能完成
- 调用差异：Gradient最后一个圆形、RText字体样式、shadow阴影、ripper中的水波纹和drawableMask有差异
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 0.0.1

## 效果演示
![输入图片说明](https://gitee.com/chinasoft_ohos/RWidgetHelper/raw/master/img/demo.gif "demo.gif")

## 安装教程

 1.在项目根目录下的build.gradle文件中，

  ```

 allprojects {
     repositories {
         maven {
             url 'https://s01.oss.sonatype.org/content/repositories/releases/'
         }
     }
 }
  ```
  2.在entry模块的build.gradle文件中，

   ```
   dependencies {
      implementation('com.gitee.chinasoft_ohos:RWidgetHelper:1.0.0')
      ......
   }
   ```
 在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件,
 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
1.基于继承openharmony原生UI组件实现圆角边框字体颜色渐变等效果


 示例xml
```
	    <com.ruffian.library.widget.RComponent
	        ohos:layout_width="100vp"
	        ohos:layout_height="100vp"

	        //背景各个状态，支持：纯颜色   渐变
	        app:background_normal="#3F51B5"
	        app:background_pressed="#3F51B5,#0000FF"
	        app:background_unable="$media:icon_star"
	        app:background_checked="$media:icon_star"
	        app:background_selected="$media:icon_star"

	        //边框颜色
	        app:border_color_normal="#FF4081"
	        app:border_color_pressed="#3F51B5"
	        app:border_color_unable="#c3c3c3"
	        app:border_color_checked="#c3c3c3"
	        app:border_color_selected="#c3c3c3"

	        //边框宽度
	        app:border_width_normal="3"
	        app:border_width_pressed="3"
	        app:border_width_unable="3"
	        app:border_width_checked="3"
	        app:border_width_selected="3"

	        //虚线边框 1.虚线边框宽度 2.虚线间隔
	        app:border_dash_width="10"
	        app:border_dash_gap="4"

	        //圆角度数 超过1/2为圆形 1.四周统一值 2.四个方向各个值
	        app:corner_radius="10"
	        app:corner_radius_top_left="10"
	        app:corner_radius_bottom_left="15"
	        app:corner_radius_bottom_right="20"
	        app:corner_radius_top_right="25"


	        //ripple
	        app:ripple="true"
	        app:ripple_color="$color/purple"
	        app:ripple_mask="$media/icon_star"

```

示例java


```
	        RComponent component = (RComponent) findComponentById(ResourceTable.Id_component);
	        //获取Helper
	        RBaseHelper helper = component.getHelper();
	        helper.setBackgroundColorNormal(new Color(Color.getIntColor("#0066FF")))
	                .setBorderColorNormal(new Color(Color.getIntColor("#0066FF")))
	                .setBorderWidthNormal(12)
	                .setCornerRadius(25);
```
 RText
 文字根据状态变色    默认/按下/不可点击/选择`

 设置字体样式

| 属性			|说明			 |
| ------------- |  :-------------|
| text_color_normal 			|   文字颜色 	默认 	|
| text_color_pressed      		|   文字颜色 	按下 	|
| text_color_unable      		| 	文字颜色 	不可点击 |
| text_color_selected      		| 	文字颜色 	选择		|
| icon_direction      			|    icon 	位置{left,top,right,bottom} |
| icon_with_text      			|   图片和文本一起居中 true/false |
| text_typeface      			|   字体样式 |

 RTextFielde

 RTextFielde 使用方法跟 RText 一致

 RDirectionLayout  /  RDependentLayout  /  RStackLayout  /  RComponent /  RConstraintLayout

 RRadioButton  / RCheckBox

| 属性			|说明			 |
| ------------- |  :-------------|
| corner_radius      			|	圆角		四周		|
| corner_radius_top_left        |   圆角		左上		|
| corner_radius_top_right 		|	圆角		右上		|
| corner_radius_bottom_left 	|   圆角		左下		|
| corner_radius_bottom_right 	|   圆角		右下 	|
| border_width		 			|   边框宽度		 	|
| border_color					|   边框颜色 		|
| is_circle	      				| 	是否圆形图片 		|


## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代

- 1.0.0

## 版权和许可信息

MIT License

Copyright (c) 2018 Ruffian-痞子